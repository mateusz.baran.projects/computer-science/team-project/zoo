package com.teamproject.zoo.presenter;


public class RouteSelectorPresenter {

    private BasePresenter presenter;

    public RouteSelectorPresenter(BasePresenter presenter, BasePresenter.IRouteView view) {
        this.presenter = presenter;
        this.presenter.setRouteView(view);
    }

    public boolean isMissionActive() {
        return presenter.isMissionActive();
    }

    public void setMissionActive(boolean isChecked) {
        presenter.setMissionActive(isChecked);
    }

    public void onConfirmRoute() {
        presenter.selectRoute();
    }

    public void showRoute(Integer id) {
        presenter.showRoute(id);
    }

    public void hideRouteSelector() {
        presenter.hideRouteSelector();
    }

    public void mapDeltaHeight(int delta) {
        presenter.getMapView().deltaHeight(delta);
    }
}
