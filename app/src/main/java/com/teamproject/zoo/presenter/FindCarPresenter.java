package com.teamproject.zoo.presenter;

public class FindCarPresenter {

    private BasePresenter presenter;

    public FindCarPresenter(BasePresenter basePresenter, BasePresenter.IFindCarView view) {
        this.presenter = basePresenter;
        this.presenter.setFindCarView(view);
    }

}
