package com.teamproject.zoo.presenter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.teamproject.zoo.model.RouteDto;
import com.teamproject.zoo.view.RouteSelectorItemFragment;

import java.util.List;

public class RouteSelectorAdapter extends FragmentPagerAdapter {
    private List<RouteDto> routes;


    public RouteSelectorAdapter(FragmentManager fragmentManager, List<RouteDto> r) {
        super(fragmentManager);
        routes = r;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return routes.size();
    }

    // Returns the fragment to display for that page. HERE ADD ROUTES
    @Override
    public Fragment getItem(int position) {
        return RouteSelectorItemFragment.newInstance(routes.get(position), "4", "5");
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
