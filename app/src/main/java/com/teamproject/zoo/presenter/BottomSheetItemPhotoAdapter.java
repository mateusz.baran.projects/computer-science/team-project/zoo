package com.teamproject.zoo.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.teamproject.zoo.R;

import java.io.File;
import java.util.List;

public class BottomSheetItemPhotoAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Integer> items;
    private List<String> customPhotoPaths;

    public BottomSheetItemPhotoAdapter(Context context, List<Integer> items, List<String> customPhotoPaths) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.items = items;
        this.customPhotoPaths = customPhotoPaths;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.pager_item_bottom_sheet_photo, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);
        if (imageView != null && items.get(position) != null) {
            if (items.get(position) > 0)
                imageView.setImageResource(items.get(position));
            else
                imageView.setImageResource(R.drawable.photo_default);
        }

        if (customPhotoPaths.get(position) != null) {
            File imgFile = new File(customPhotoPaths.get(position));

            Picasso.with(context)
                    .load(imgFile)
                    .fit()
                    .centerCrop()
                    .into(imageView);
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
