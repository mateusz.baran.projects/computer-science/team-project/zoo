package com.teamproject.zoo.presenter;

import com.teamproject.zoo.model.BottomSheetItemConstant;
import com.teamproject.zoo.model.IBottomSheetItem;
import com.teamproject.zoo.model.MarkerDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BottomSheetLayoutHelper {


    @SuppressWarnings("unchecked")
    public static List<HashMap<String, Object>> getLayouts(IBottomSheetItem bottomSheetItem, String name) {
        Map<String, Object> layouts = bottomSheetItem.getLayouts();
        if (layouts != null) {
            List<?> layoutList = (List<?>) layouts.get(name);
            if (layoutList != null && layoutList.size() > 0)
                return (List<HashMap<String, Object>>) layouts.get(name);
        }
        return new ArrayList<>();
    }

    public static String getLayoutID(HashMap<String, Object> layout) {
        String id = (String) layout.get(BottomSheetItemConstant.LAYOUTS_ID);
        if (id != null)
            return id;
        return "";
    }

    @SuppressWarnings("unchecked")
    public static Map<String, String> getLayoutContents(HashMap<String, Object> layout) {
        Map<String, String> contents = (Map<String, String>) layout.get(BottomSheetItemConstant.CONTENTS);
        if (contents != null)
            return contents;
        return new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    public static Map<String, String> getLayoutImages(HashMap<String, Object> layout) {
        Map<String, String> images = (Map<String, String>) layout.get(BottomSheetItemConstant.IMAGES);
        if (images != null)
            return images;
        return new HashMap<>();
    }

    public static String getBottomSheetItemTitle(IBottomSheetItem bottomSheetItem) {
        List<HashMap<String, Object>> layouts = getLayouts(bottomSheetItem, BottomSheetItemConstant.LAYOUTS_BOTTOM_SHEET);
        for (HashMap<String, Object> layout : layouts) {
            Map<String, String> contents = getLayoutContents(layout);
            for (String contentKey : contents.keySet())
                if (BottomSheetItemConstant.TITLE_KEY.equals(contentKey))
                    return contents.get(contentKey);

        }
        return "";
    }

    public static String getBottomSheetItemHabitat(IBottomSheetItem bottomSheetItem) {
        List<HashMap<String, Object>> layouts = getLayouts(bottomSheetItem, BottomSheetItemConstant.LAYOUTS_BOTTOM_SHEET);
        for (HashMap<String, Object> layout : layouts) {
            Map<String, String> contents = getLayoutContents(layout);
            for (String contentKey : contents.keySet())
                if (BottomSheetItemConstant.ANIMAL_HABITAT_KEY.equals(contentKey))
                    return contents.get(contentKey);

        }
        return "";
    }
}
