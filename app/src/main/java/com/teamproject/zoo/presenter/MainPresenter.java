package com.teamproject.zoo.presenter;

public class MainPresenter {

    private BasePresenter presenter;

    public MainPresenter(BasePresenter presenter, BasePresenter.IMainView view) {
        this.presenter = presenter;
        this.presenter.setMainView(view);
    }

    public boolean isSelectedRoute() {
        return presenter.isSelectedRoute();
    }

    public void onMenuShowRouteSelector() {
        presenter.hideMarkerInfo();
        presenter.showRouteSelector();
    }

    public void onMenuClearSelectedRoute() {
        presenter.clearSelectedRoute();
    }

    public void onMenuHome() {
        presenter.toolbarHomeButton(false);
    }

    public void onNavigationDrawerShowParking() {
        presenter.hideMarkerInfo();
        presenter.showParking();
    }

    public boolean onBackArrow() {
        return presenter.toolbarHomeButton(true);
    }

    public void onMenuSearch() {
        presenter.showSearchPanel();
    }

    public void onNavigationDrawerFindCar() {
        presenter.showFindCarPanel();
    }

    public void onNavigationDrawerInfo() {
        presenter.openInfo();
    }

    public void onNavigationDrawerTickets() {
        presenter.openTickets();
    }

    public void onNavigationDrawerOpeningHours() {
        presenter.openOpeningHours();
    }

    public void onNavigationDrawerMissionList() {
        presenter.openMissionList();
    }

    public void onNavigationDrawerBabyAnimals() {
        presenter.openBabyAnimals();
    }

    public void onNavigationDrawerFeeding() {
        presenter.openFeeding();
    }
}
