package com.teamproject.zoo.presenter;

import com.teamproject.zoo.model.ToolbarMode;
import com.teamproject.zoo.model.ZooMap;

public class MapPresenter {

    private BasePresenter presenter;
    private ZooMap zooMap;
    private BasePresenter.IMapView view;

    public MapPresenter(BasePresenter presenter, BasePresenter.IMapView view) {
        this.zooMap = new ZooMap();
        this.presenter = presenter;
        this.presenter.setMapView(view);
        this.view = view;
    }

    public ZooMap getZooMap() {
        return zooMap;
    }

    public void initMapElements() {
        presenter.initMapElements();
    }

    public void onZoomChangeEvent(Float currentZoom) {
        presenter.setCurrentZoom(currentZoom);
    }

    public void onMarkerClick(Integer id) {
        presenter.showMarkerInfo(id);
    }

    public void onMapClick() {
        if (presenter.getMode() == BasePresenter.MODE_AUGMENTED_REALITY)
            presenter.clearMarkerInfo();
        else
            presenter.toggleMarkerInfoAndSearchBar();
    }

    public void onMapLoaded() {
        presenter.getMainView().fadeSplashScreen();
    }

    public void augmentedRealityAnimationProgress(float progress) {
        presenter.getMainView().setToolbarAlpha((int) ((1 - progress) * 255));
        if (progress > .5) {
            presenter.setToolbarMode(ToolbarMode.AUGMENTED_REALITY);
            presenter.getBottomSheetView().hideFABAugmentedReality();
        } else {
            presenter.setToolbarMode(ToolbarMode.NORMAL);
            presenter.getBottomSheetView().showFABAugmentedReality();
        }
    }

    public void augmentedRealityFinished() {
        presenter.getAugmentedRealityView().stop();
    }
}
