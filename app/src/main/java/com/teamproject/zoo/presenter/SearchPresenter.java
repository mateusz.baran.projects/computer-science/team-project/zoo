package com.teamproject.zoo.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teamproject.zoo.model.CheckboxTypeConstant;
import com.teamproject.zoo.model.CheckboxTypeDto;

import java.util.ArrayList;
import java.util.List;

public class SearchPresenter {

    private static final String TAG = "SearchPresenter";

    private BasePresenter presenter;
    private BasePresenter.ISearchView view;

    private FirebaseFirestore db;

    private List<CheckboxTypeDto> checkboxGroups;

    private boolean isStateCollapsed;
    private boolean requireHide;
    private boolean requireShow;
    private boolean isBottomSheetCollapsed;
    private boolean isFirstStep;


    public SearchPresenter(BasePresenter basePresenter, BasePresenter.ISearchView view) {
        this.presenter = basePresenter;
        this.presenter.setSearchView(view);
        this.view = view;

        this.checkboxGroups = new ArrayList<>();
        this.db = FirebaseFirestore.getInstance();

        this.requireHide = false;
        this.requireShow = false;
        this.isStateCollapsed = true;
        this.isBottomSheetCollapsed = false;
        this.isFirstStep = true;

        initCheckboxGroups();
    }

    private void initCheckboxGroups() {
        Log.d(TAG, "initCheckboxGroups: called.");

        db.collection(CheckboxTypeConstant.FB_COLLECTION_PATH)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, "initCheckboxGroups: " + document.getId() + " => " + document.getData());
                                CheckboxTypeDto group = document.toObject(CheckboxTypeDto.class);
                                checkboxGroups.add(group);
                            }

                            view.setupCheckboxListView(checkboxGroups);
                        } else {
                            Log.e(TAG, "initCheckboxGroups: ", task.getException());
                        }
                    }
                });
    }

    public void onCheckboxTypeClick(String type, boolean isChecked) {
        if (isChecked)
            presenter.getCheckboxActiveTypes().add(type);
        else
            presenter.getCheckboxActiveTypes().remove(type);

        presenter.updateMarkersVisibility();
    }

    public boolean isCheckboxActive(String type) {
        return presenter.getCheckboxActiveTypes().contains(type);
    }

    public void onListItemClick(Integer markerDtoID) {
        view.collapseListView();
        presenter.showMarkerInfo(markerDtoID);
        presenter.getMapView().centerMarkerPosition(markerDtoID);
    }

    public void onStateCollapsed() {
        if (requireHide)
            view.searchViewAnimator(false);
        else if (requireShow)
            view.searchViewAnimator(true);

        requireHide = false;
        requireShow = false;
        isStateCollapsed = true;
    }

    public void onSliding(float progress) {
        isStateCollapsed = false;
        progress = progress > 1 ? 1 : progress < 0 ? 0 : progress;
        view.setRoundedCorners((int) ((1 - progress) * view.getVisiblePeek()));
        manageFABAndBottomSheet(progress);
    }

    public void hide() {
        if (isStateCollapsed) {
            view.searchViewAnimator(false);
        } else {
            view.collapseListView();
            requireHide = true;
        }
    }

    public void show() {
        if (isStateCollapsed) {
            view.searchViewAnimator(true);
        } else {
            view.collapseListView();
            requireShow = true;
        }
    }

    public void manageFABMyLocation(float posY, int toolbarHeight, int fabMargin) {
        presenter.getMapView().setTopMarginFABMyLocation(Math.max((int) posY, toolbarHeight) + fabMargin);
    }

    private void manageFABAndBottomSheet(float slideProgress) {
        if (isFirstStep)
            isBottomSheetCollapsed = presenter.getBottomSheetView().isStateCollapsed();

        if (slideProgress > 0) {
            presenter.getMapView().hideFABMyLocation();
            presenter.getBottomSheetView().hideFABAugmentedReality();

            if (isBottomSheetCollapsed && isFirstStep)
                presenter.getBottomSheetView().hideBottomFragment();

            isFirstStep = false;
        } else {
            presenter.getMapView().showFABMyLocation();
            presenter.getBottomSheetView().showFABAugmentedReality();

            if (isBottomSheetCollapsed)
                presenter.getBottomSheetView().showBottomFragment();

            isFirstStep = true;
        }
    }

}
