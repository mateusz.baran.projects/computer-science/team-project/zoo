package com.teamproject.zoo.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.SearchListItem;

import java.util.List;

public class SearchListAdapter extends ArrayAdapter<SearchListItem> {

    private List<SearchListItem> data;
    private Context context;

    public SearchListAdapter(List<SearchListItem> data, Context context) {
        super(context, R.layout.search_list_item, data);
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        SearchListItem searchListItem = data.get(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.search_list_item, parent, false);
            viewHolder.imageView = convertView.findViewById(R.id.search_list_item_photo);
            viewHolder.titleTextView = convertView.findViewById(R.id.search_list_item_title);
            viewHolder.descriptionTextView = convertView.findViewById(R.id.search_list_item_description);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (searchListItem.getImage() != null)
            viewHolder.imageView.setImageDrawable(searchListItem.getImage());
        viewHolder.titleTextView.setText(searchListItem.getTitle());
        viewHolder.descriptionTextView.setText(searchListItem.getHabitat());

        return convertView;
    }

    public Integer getMarkerDtoID(int position) {
        return data.get(position).getId();
    }

    private static class ViewHolder {
        ImageView imageView;
        TextView titleTextView;
        TextView descriptionTextView;
    }
}
