package com.teamproject.zoo.presenter;


import android.annotation.SuppressLint;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teamproject.zoo.model.CheckboxTypeConstant;
import com.teamproject.zoo.model.CheckboxTypeDto;
import com.teamproject.zoo.model.IBottomSheetItem;
import com.teamproject.zoo.model.MarkerAR;
import com.teamproject.zoo.model.MarkerConstant;
import com.teamproject.zoo.model.MarkerDto;
import com.teamproject.zoo.model.MarkerType;
import com.teamproject.zoo.model.NavDrawerDto;
import com.teamproject.zoo.model.RouteConstant;
import com.teamproject.zoo.model.RouteDto;
import com.teamproject.zoo.model.ToolbarMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class BasePresenter {

    final static int MODE_NORMAL = 1;
    final static int MODE_ROUTE_SELECTION = 2;
    final static int MODE_SIGHTSEEING = 3;
    final static int MODE_PARKING = 4;
    final static int MODE_FIND_MY_CAR = 5;
    final static int MODE_AUGMENTED_REALITY = 6;

    private IMainView mainView;
    private IMapView mapView;
    private IBottomSheetView bottomSheetView;
    private IRouteView routeView;
    private ISearchView searchView;
    private IAugmentedRealityView augmentedRealityView;
    private IFindCarView findCarView;

    private int mode;
    private int toolbarMode;
    private int toolbarModeCache;

    private Float currentZoom;

    private boolean isShownBottomSheet;
    private boolean isMissionActive;

    private MarkerDto activeMarker;
    private MarkerDto activeMission;
    private RouteDto activeRoute;
    private RouteDto selectedRoute;

    private final Map<Integer, MarkerDto> markers;
    private final Map<Integer, RouteDto> routes;
    private final Map<Integer, NavDrawerDto> navDrawers;
    private final Map<String, List<IBottomSheetItem>> markerGroups;
    private final Map<Integer, Boolean> missionAccomplished;
    private final Set<String> checkboxActiveTypes;
    private final Set<CheckboxTypeDto> checkboxAllTypes;

    private FirebaseFirestore db;

    @SuppressLint("UseSparseArrays")
    public BasePresenter() {
        this.mode = MODE_NORMAL;
        this.toolbarMode = ToolbarMode.NORMAL;
        this.toolbarModeCache = ToolbarMode.NORMAL;

        this.currentZoom = null;

        this.isShownBottomSheet = false;
        this.isMissionActive = true;

        this.activeMarker = null;

        this.activeMission = null;
        this.activeRoute = null;
        this.selectedRoute = null;

        this.markers = new HashMap<>();
        this.routes = new HashMap<>();
        this.navDrawers = new HashMap<>();
        this.markerGroups = new HashMap<>();
        this.missionAccomplished = new HashMap<>();
        this.checkboxActiveTypes = new HashSet<>();
        this.checkboxAllTypes = new HashSet<>();

        this.db = FirebaseFirestore.getInstance();
    }


    // Getters and Setters

    void setMainView(IMainView mainView) {
        this.mainView = mainView;
    }

    void setMapView(IMapView mapView) {
        this.mapView = mapView;
    }

    void setBottomSheetView(IBottomSheetView bottomSheetView) {
        this.bottomSheetView = bottomSheetView;
    }

    void setRouteView(IRouteView routeView) {
        this.routeView = routeView;
    }

    void setAugmentedRealityView(IAugmentedRealityView augmentedRealityView) {
        this.augmentedRealityView = augmentedRealityView;
    }

    void setSearchView(ISearchView searchView) {
        this.searchView = searchView;
    }

    void setFindCarView(IFindCarView findCarView) {
        this.findCarView = findCarView;
    }

    IMainView getMainView() {
        return mainView;
    }

    IMapView getMapView() {
        return mapView;
    }

    IBottomSheetView getBottomSheetView() {
        return bottomSheetView;
    }

    IRouteView getRouteView() {
        return routeView;
    }

    ISearchView getSearchView() {
        return searchView;
    }

    IAugmentedRealityView getAugmentedRealityView() {
        return augmentedRealityView;
    }

    public IFindCarView getFindCarView() {
        return findCarView;
    }

    boolean isShownBottomSheet() {
        return isShownBottomSheet;
    }

    void setShownBottomSheet(boolean isShownMarkerInfo) {
        this.isShownBottomSheet = isShownMarkerInfo;
    }

    MarkerDto getActiveMarker() {
        return activeMarker;
    }

    public Map<Integer, MarkerDto> getMarkers() {
        return markers;
    }

    int getMode() {
        return this.mode;
    }

    Set<String> getCheckboxActiveTypes() {
        return checkboxActiveTypes;
    }

    public Set<CheckboxTypeDto> getCheckboxAllTypes() {
        return checkboxAllTypes;
    }

    // Map

    private void setMode(int mode) {
        this.mode = mode;
        clearMarkerInfo();
        manageFABAugmentedReality();
    }

    void updateMarkersVisibility() {
        for (MarkerDto markerDto : markers.values()) {
            boolean visibility = false;

            switch (mode) {
                case MODE_NORMAL:
                    visibility = isNormalMarker(markerDto) || isCheckboxTypeMarker(markerDto);
                    break;
                case MODE_ROUTE_SELECTION:
                    visibility = isActiveRouteMissionMarker(markerDto);
                    break;
                case MODE_SIGHTSEEING:
                    visibility = isNormalMarker(markerDto)
                            || isActiveMissionMarker(markerDto)
                            || isCheckboxTypeMarker(markerDto);
                    break;
                case MODE_PARKING:
                    visibility = isParkingMarker(markerDto);
                    break;
                case MODE_FIND_MY_CAR:
                    visibility = isSavedCarLocationMarker(markerDto);
                    break;
                case MODE_AUGMENTED_REALITY:
                    visibility = isAugmentedRealityMarker(markerDto);
                    break;
            }

            mapView.setMarkerVisibility(markerDto.getId(), visibility);
            if (activeMarker == markerDto && !visibility)
                mapView.deactivateMarker(markerDto);
        }
    }

    private boolean isActiveMissionMarker(MarkerDto markerDto) {
        return MarkerType.MISSION.equals(markerDto.getType())
                && isMissionActive
                && activeMission != null
                && markerDto.getId() == activeMission.getId();
    }

    private boolean isActiveRouteMissionMarker(MarkerDto markerDto) {
        return MarkerType.MISSION.equals(markerDto.getType())
                && isMissionActive
                && activeRoute != null
                && activeRoute.getGroup().equals(markerDto.getGroup());
    }

    private boolean isParkingMarker(MarkerDto markerInfo) {
        return MarkerType.PARKING.equals(markerInfo.getType());
    }

    private boolean isNormalMarker(MarkerDto markerDto) {
        return MarkerType.NORMAL.equals(markerDto.getType())
                && (markerDto.getZoom().get(MarkerConstant.ZOOM_ACTIVATION) <= currentZoom && currentZoom <= markerDto.getZoom().get(MarkerConstant.ZOOM_DEACTIVATION)
                || activeMarker != null && markerDto.getId() == activeMarker.getId());
    }

    private boolean isSavedCarLocationMarker(MarkerDto markerDto){
        return false;
    }

    private boolean isCheckboxTypeMarker(MarkerDto markerDto) {
        return checkboxActiveTypes.contains(markerDto.getType());
    }

    public boolean isAugmentedRealityMarker(MarkerDto markerDto) {
        return isNormalMarker(markerDto)
                || isActiveMissionMarker(markerDto)
                || isCheckboxTypeMarker(markerDto);
    }

    public void initMapElements() {
        initMarkers();
        initRoutes();
        initNavDrawers();
        initCheckboxGroups();
    }

    private void initMarkers() {
        db.collection(MarkerConstant.FB_COLLECTION_PATH)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("initMarkers", document.getId() + " => " + document.getData());
                                MarkerDto markerDto = document.toObject(MarkerDto.class);
                                markers.put(markerDto.getId(), markerDto);
                            }
                            for (MarkerDto markerDto : markers.values()) {
                                if (!markerGroups.containsKey(markerDto.getGroup())) {
                                    markerGroups.put(markerDto.getGroup(), new ArrayList<IBottomSheetItem>());
                                }
                                Objects.requireNonNull(markerGroups.get(markerDto.getGroup())).add(markerDto);
                            }

                            for (MarkerDto markerDto : markers.values()) {
                                mapView.addMarker(markerDto);
                            }

                            initMissionAccomplished();
                            updateMarkersVisibility();
                        } else {
                            Log.e("initMarkers", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private void initRoutes() {
        db.collection(RouteConstant.FB_COLLECTION_PATH)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("initRoutes", document.getId() + " => " + document.getData());
                                RouteDto routeDto = document.toObject(RouteDto.class);
                                routes.put(routeDto.getId(), routeDto);
                            }
                        } else {
                            Log.e("initRoutes", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private void initNavDrawers() {
        db.collection("navDrawer")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("initNavDrawers", document.getId() + " => " + document.getData());
                                NavDrawerDto navDrawerDto = document.toObject(NavDrawerDto.class);
                                navDrawers.put(navDrawerDto.getId(), navDrawerDto);
                            }

                            showNavDrawerItemsWithName("info");

                        } else {
                            Log.e("initNavDrawers", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private void initCheckboxGroups() {

        db.collection(CheckboxTypeConstant.FB_COLLECTION_PATH)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("initCheckbox", "initCheckboxGroups: " + document.getId() + " => " + document.getData());
                                CheckboxTypeDto group = document.toObject(CheckboxTypeDto.class);
                                checkboxAllTypes.add(group);
                            }
                        } else {
                            Log.e("initCheckbox", "initCheckboxGroups: ", task.getException());
                        }
                    }
                });
    }

    private void initMissionAccomplished() {
        for (MarkerDto markerDto : markers.values())
            if (MarkerType.MISSION.equals(markerDto.getType()))
                missionAccomplished.put(markerDto.getId(), bottomSheetView.isMissionAccomplished(markerDto.getId()));
    }

    public void setCurrentZoom(Float currentZoom) {
        this.currentZoom = currentZoom;
        updateMarkersVisibility();
    }

    public void showParking() {
        setMode(MODE_PARKING);
        setToolbarMode(ToolbarMode.PARKING);
        mapView.setCameraForParkingMode();
        mapView.clearRoutePolyline();
    } // parking drawer click

    public void hideParking() {
        setMode(MODE_NORMAL);
        setToolbarMode(ToolbarMode.NORMAL);
        mapView.setCameraForNormalMode();
        mainView.clearSelectionFromNavigationDrawer();
        if (activeRoute != null)
            mapView.setRoutePolyline(activeRoute);
    } // parking back arrow click


    // Toolbar

    public void setToolbarMode(int toolbarMode) {

        switch (toolbarMode) {
            case ToolbarMode.NORMAL:
                mainView.setToolbarNormal();
                toolbarModeCache = toolbarMode;
                break;
            case ToolbarMode.ROUTE_SELECTION:
                mainView.setToolbarRouteSelection();
                toolbarModeCache = toolbarMode;
                break;
            case ToolbarMode.BOTTOM_PANEL_EXPANDED_STATE_INFO:
                mainView.setToolbarBottomPanelExpandedStateInfo();
                break;
            case ToolbarMode.BOTTOM_PANEL_MIDDLE_STATE_INFO:
                mainView.setToolbarBottomPanelMiddleStateInfo();
                break;
            case ToolbarMode.PARKING:
                mainView.setToolbarParking();
                toolbarModeCache = toolbarMode;
                break;
            case ToolbarMode.AUGMENTED_REALITY:
                mainView.setToolbarAugmentedReality();
                toolbarModeCache = toolbarMode;
                break;
            case ToolbarMode.SEARCH:
                mainView.setToolbarSearch();
                toolbarModeCache = toolbarMode;
                break;
            case ToolbarMode.FIND_CAR:
                mainView.setToolbarFindCar();
                toolbarModeCache = toolbarMode;
                break;
        }
        this.toolbarMode = toolbarMode;
    }

    public void resetToolbarMode() {
        setToolbarMode(toolbarModeCache);
    }

    public boolean toolbarHomeButton(boolean backButton) {
        switch (toolbarMode) {
            case ToolbarMode.NORMAL:
                if (!backButton) {
                    mainView.openNavigationDrawer();
                } else if (mainView.isOpenNavigationDrawer()) {
                    mainView.closeNavigationDrawer();
                    return false;
                }
                return true;
            case ToolbarMode.BOTTOM_PANEL_EXPANDED_STATE_INFO:
            case ToolbarMode.BOTTOM_PANEL_MIDDLE_STATE_INFO:
                if (mode == MODE_AUGMENTED_REALITY) hideMarkerInfo();
                else collapseMarkerInfo();
                return false;
            case ToolbarMode.ROUTE_SELECTION:
                hideRouteSelector();
                return false;
            case ToolbarMode.PARKING:
                hideParking();
                return false;
            case ToolbarMode.AUGMENTED_REALITY:
                hideAugmentedReality();
                return false;
            case ToolbarMode.SEARCH:
                hideSearchPanel();
                return false;
            case ToolbarMode.FIND_CAR:
                hideFindCarPanel();
                return false;

        }
        return true;
    }


    // Marker

    private void showMarkerInfoForCurrentMode() {
        switch (mode) {
            case MODE_NORMAL:
            case MODE_PARKING:
            case MODE_SIGHTSEEING:
                List<IBottomSheetItem> group = markerGroups.get(activeMarker.getGroup());
                if (group != null)
                    bottomSheetView.showBottomSheetItem(group, group.indexOf(activeMarker));
                break;
            case MODE_AUGMENTED_REALITY:
                List<IBottomSheetItem> group2 = markerGroups.get(activeMarker.getGroup());
                if (group2 != null)
                    bottomSheetView.showBottomSheetItem(group2, group2.indexOf(activeMarker));
                break;
            case MODE_FIND_MY_CAR:
                findCarView.openNavigationToSavedLocation();
                break;
            default:
                // mapView.showMarkerWindow(activeMarker.getId());
                break;
        }
    } // show marker info fragment

    public void toggleMarkerInfoAndSearchBar() {
        if (activeMarker != null) {
            if (isShownBottomSheet) {
                searchView.searchViewAnimator(false);
                hideMarkerInfo();
            } else {
                searchView.searchViewAnimator(true);
                showMarkerInfoForCurrentMode();
            }
        }
    }

    public void showMarkerInfo(Integer id) {
        mainView.clearSelectionFromNavigationDrawer();

        if (mode == BasePresenter.MODE_AUGMENTED_REALITY && (activeMarker == null || activeMarker.getId() != id))
            showMarker(id);
        else if (showMarker(id) || mode == MODE_FIND_MY_CAR)
            showMarkerInfoForCurrentMode();
    }

    public boolean showMarker(Integer id) {
        MarkerDto marker = markers.get(id);
        if (marker != null) {
            if (marker == activeMarker) {
                mapView.centerMarkerPosition(id);
            } else {
                if (activeMarker != null)
                    mapView.deactivateMarker(activeMarker);

                activeMarker = marker;

                if (MarkerType.MISSION.equals(activeMarker.getType()))
                    activeMission = activeMarker;

                mapView.activateMarker(activeMarker);
                updateMarkersVisibility();
            }
            return true;
        }
        return false;
    }

    public void collapseMarkerInfo() {
        bottomSheetView.collapseBottomFragment();
    }

    public void hideMarkerInfo() {
        mainView.clearSelectionFromNavigationDrawer();
        bottomSheetView.hideBottomFragment();
    }

    public void clearMarkerInfo() {
        if (activeMarker != null) {
            mapView.deactivateMarker(activeMarker);
            activeMarker = null;
            hideMarkerInfo();
            updateMarkersVisibility();
        }
    }

    public void showNextMissionMarker() {
        if (selectedRoute != null && isMissionActive) {
            if (activeMission == null) {
                Integer missionID = getFirstNotAccomplishedMissionID();
                if (missionID != null)
                    showMarkerInfo(missionID);
            } else if (activeMission == activeMarker) {
                Integer missionID = getNextNotAccomplishedMissionID();
                if (missionID != null)
                    showMarkerInfo(missionID);
            }
        }

    }

    private Integer getFirstNotAccomplishedMissionID() {
        List<IBottomSheetItem> group = markerGroups.get(activeRoute.getGroup());
        if (group != null) {
            for (int i = 0; i < group.size(); i++) {
                IBottomSheetItem markerDto = group.get(i);
                if (markerDto != null) {
                    Boolean isMissionAccomplished = missionAccomplished.get(markerDto.getId());
                    if (isMissionAccomplished != null && !isMissionAccomplished)
                        return markerDto.getId();
                }
            }
            if (group.size() > 0)
                return group.get(0).getId();
        }

        return null;
    }

    private Integer getNextNotAccomplishedMissionID() {
        Boolean isMissionAccomplished = missionAccomplished.get(activeMission.getId());
        if (isMissionAccomplished != null
                && !isMissionAccomplished
                && bottomSheetView.isMissionAccomplished(activeMission.getId())) {
            missionAccomplished.put(activeMission.getId(), true);
            List<IBottomSheetItem> group = markerGroups.get(activeRoute.getGroup());
            if (group != null) {
                int nextIndex = group.indexOf(activeMission) + 1;
                if (0 < nextIndex && nextIndex < group.size()) {
                    IBottomSheetItem nextMission = group.get(nextIndex);
                    return nextMission.getId();
                }
            }
        }
        return null;
    }

    public void manageFABAugmentedReality() {
        if (mode == MODE_NORMAL || mode == MODE_SIGHTSEEING)
            bottomSheetView.showFABAugmentedReality();
        else
            bottomSheetView.hideFABAugmentedReality();
    }


    // Route

    public boolean isSelectedRoute() {
        return selectedRoute != null;
    }

    public void selectRoute() {
        selectedRoute = activeRoute;
        activeMission = null;

        hideRouteSelector();
    } // route fragment select click

    public void showRoute(Integer id) {
        activeRoute = routes.get(id);
        assert activeRoute != null;

        mapView.setRoutePolyline(activeRoute);
        updateMarkersVisibility();
    }

    public void clearSelectedRoute() {
        setMode(MODE_NORMAL);
        setToolbarMode(ToolbarMode.NORMAL);

        activeMission = null;
        activeRoute = null;
        selectedRoute = null;

        routeView.hideRouteSelector();
        mapView.clearRoutePolyline();
        updateMarkersVisibility();
    }

    public void showRouteSelector() {
        setMode(MODE_ROUTE_SELECTION);
        setToolbarMode(ToolbarMode.ROUTE_SELECTION);

        ArrayList<RouteDto> routeDtos = new ArrayList<>(routes.values());
        Integer id = null;

        if (isSelectedRoute()) {
            id = routeDtos.indexOf(selectedRoute);
        } else if (routeDtos.size() > 0) {
            id = 0;
        }

        routeView.showRouteSelector(routeDtos, id);
        updateMarkersVisibility();
    }

    public void hideRouteSelector() {
        if (selectedRoute != null) {
            setMode(MODE_SIGHTSEEING);
            setToolbarMode(ToolbarMode.NORMAL);

            if (selectedRoute != activeRoute) {
                activeRoute = selectedRoute;
                mapView.setRoutePolyline(selectedRoute);
            }

            routeView.hideRouteSelector();
            showNextMissionMarker();
            updateMarkersVisibility();
        } else {
            clearSelectedRoute();
        }
    }

    public boolean isMissionActive() {
        return isMissionActive;
    }

    public void setMissionActive(boolean isMissionActive) {
        this.isMissionActive = isMissionActive;
        updateMarkersVisibility();
    }

    public void showSearchPanel() {
        setToolbarMode(ToolbarMode.SEARCH);

        searchView.showSearchPanel(new ArrayList<>(markers.values()));
    }

    public void hideSearchPanel() {
        searchView.hideSearchPanel();

        setToolbarMode(ToolbarMode.NORMAL);
    }


    // Augmented Reality

    public void showAugmentedReality() {
        if (mapView.isAnimationDone()) {
            augmentedRealityView.start();
        }
    }

    public void showAugmentedRealityConfirmed() {
        setMode(MODE_AUGMENTED_REALITY);
        bottomSheetView.hideBottomFragment();
        searchView.hideSearchPanel();
        mapView.setAugmentedRealityMapMode(true);
    }

    public void hideAugmentedReality() {
        if (mapView.isAnimationDone()) {
            setMode(MODE_NORMAL);
            augmentedRealityView.stopWorker();
            mapView.setAugmentedRealityMapMode(false);
        }
    }

    // Find Car

    public void showFindCarPanel() {
        setToolbarMode(ToolbarMode.FIND_CAR);
        setMode(MODE_FIND_MY_CAR);
        mapView.setCameraForFindCarMode();
        mapView.addCarMarker(findCarView.getSavedCarMarker());
        findCarView.showFindCarPanel();
        mapView.clearRoutePolyline();
    }

    public void hideFindCarPanel() {
        setToolbarMode(ToolbarMode.NORMAL);
        setMode(MODE_NORMAL);
        mapView.setCameraForNormalMode();
        mapView.removeCarMarker();
        findCarView.hideFindCarPanel();
        mainView.clearSelectionFromNavigationDrawer();
        if (activeRoute != null)
            mapView.setRoutePolyline(activeRoute);
    }

    // Other options

    private void showNavDrawerItemsWithName(String name) {
        List<IBottomSheetItem> data = new ArrayList<>();
        for (NavDrawerDto item : navDrawers.values()){
            if (item.getName().equals(name))
                data.add(item);
        }
        bottomSheetView.showBottomSheetItem(data, 0);
    }

    public void openInfo() {
        clearMarkerInfo();
        showNavDrawerItemsWithName("info");
    }

    public void openTickets() {
        clearMarkerInfo();
        showNavDrawerItemsWithName("ticket");
    }

    public void openOpeningHours() {
        clearMarkerInfo();
        showNavDrawerItemsWithName("opening_hours");
    }

    public void openMissionList() {
        clearMarkerInfo();
        showNavDrawerItemsWithName("mission_list");
    }

    public void openBabyAnimals() {
        clearMarkerInfo();
        showNavDrawerItemsWithName("baby_animals");
    }

    public void openFeeding() {
        clearMarkerInfo();
        showNavDrawerItemsWithName("feeding_hours");
    }


    // Interfaces

    public interface IMainView {
        void fadeSplashScreen();

        void setToolbarAlpha(int alpha);

        void setToolbarColor(int color);

        void setToolbarTitle(int alpha, String title);

        void setToolbarNormal();

        void setToolbarRouteSelection();

        void setToolbarBottomPanelExpandedStateInfo();

        void setToolbarBottomPanelMiddleStateInfo();

        void setToolbarParking();

        void setToolbarAugmentedReality();

        void setToolbarSearch();

        void setToolbarFindCar();

        void clearSelectionFromNavigationDrawer();

        void openNavigationDrawer();

        void closeNavigationDrawer();

        boolean isOpenNavigationDrawer();
    }

    public interface IRouteView {
        void showRouteSelector(List<RouteDto> routes, Integer index);

        void hideRouteSelector();
    }

    public interface IBottomSheetView {
        void showBottomSheetItem(List<IBottomSheetItem> bottomSheetItems, Integer index);

        void setRoundedCorners(int radius, int photoRadius);

        void expandBottomFragment();

        void showBottomFragment();

        void hideBottomFragment();

        void collapseBottomFragment();

        boolean isStateCollapsed();

        void setScrolling(boolean isScrollable);

        void smoothScrollTo(int x, int y);

        void setPagingEnabled(Boolean isPageable);

        boolean isMissionAccomplished(Integer markerID);

        void showFABAugmentedReality();

        void hideFABAugmentedReality();

        void setBottomMarginFABAugmentedReality(int margin);

        int getHeight();

        int getFabMargin();

    }

    public interface IMapView {
        void addMarker(MarkerDto markerDto);

        void addCarMarker(MarkerOptions markerOptions);

        void removeCarMarker();

        void setMarkerVisibility(Integer id, boolean isVisible);

        void activateMarker(MarkerDto markerDto);

        void deactivateMarker(MarkerDto markerDto);

        void centerMarkerPosition(Integer id);

        void showMarkerWindow(Integer id);

        void setCameraForParkingMode();

        void setCameraForFindCarMode();

        void setCameraForNormalMode();

        void setCameraForAugmentedRealityMode(float bearing, float tilt);

        void setRoutePolyline(RouteDto routeDto);

        void clearRoutePolyline();

        void showFABMyLocation();

        void hideFABMyLocation();

        void setTopMarginFABMyLocation(int margin);

        Location getMyLocation();

        boolean isAnimationDone();

        void setAugmentedRealityMapMode(boolean enable);

        void deltaHeight(int delta);
    }

    public interface IAugmentedRealityView {

        void start();

        void stop();

        void stopWorker();

        float getAzimuth();

        float getAltitude();

        float getHorizontalViewAngle();

        float getVerticalViewAngle();

        void addItem(MarkerAR markerAR);

        void updateItemParams(MarkerAR markerAR);

        void updateTitleBar(MarkerAR markerAR, boolean force);
    }

    public interface ISearchView {
        void showSearchPanel(List<MarkerDto> markerDtos);

        void hideSearchPanel();

        void setupCheckboxListView(List<CheckboxTypeDto> checkboxTypeDtos);

        void searchViewAnimator(boolean enable);

        void collapseListView();

        void setRoundedCorners(int radius);

        int getVisiblePeek();
    }

    public interface IFindCarView {
        void showFindCarPanel();

        void hideFindCarPanel();

        MarkerOptions getSavedCarMarker();

        void openNavigationToSavedLocation();
    }
}
