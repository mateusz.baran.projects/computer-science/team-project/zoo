package com.teamproject.zoo.presenter;


import android.location.Location;
import android.view.WindowManager;

import com.teamproject.zoo.model.MarkerAR;
import com.teamproject.zoo.model.MarkerConstant;
import com.teamproject.zoo.model.MarkerDto;

import java.util.ArrayList;
import java.util.List;

import static com.teamproject.zoo.model.MarkerAR.MAX_OPACITY;
import static com.teamproject.zoo.model.MarkerAR.MAX_SIZE;
import static com.teamproject.zoo.model.MarkerAR.MIN_OPACITY;
import static com.teamproject.zoo.model.MarkerAR.MIN_SIZE;
import static com.teamproject.zoo.view.MapFragment.AUGMENTED_REALITY_MARGIN;

public class AugmentedRealityPresenter {

    private static final int Y_MARGIN = 50;

    public static final int DELAY = 33;

    private static final int MIN_ELEVATION = 1;
    private static final int TILT = 60;
    private static final int TILT_MARGIN = 15;

    private BasePresenter presenter;
    private BasePresenter.IAugmentedRealityView view;
    private List<MarkerAR> markerARList;
    private MarkerDto activeMarkerDto;
    private float viewHorizontalAngle;
    private float viewVerticalAngle;
    private int width;

    private int maxY;
    private int minY;

    public AugmentedRealityPresenter(BasePresenter presenter, BasePresenter.IAugmentedRealityView view) {
        this.presenter = presenter;
        this.presenter.setAugmentedRealityView(view);
        this.view = view;

    }

    public void setup(WindowManager windowManager, int titleBarHeight) {
        this.markerARList = new ArrayList<>();
        for (MarkerDto markerDto : presenter.getMarkers().values()) {
            MarkerAR markerAR = new MarkerAR(markerDto);
            markerAR.setTitle(BottomSheetLayoutHelper.getBottomSheetItemTitle(markerDto));
            markerAR.setHabitat(BottomSheetLayoutHelper.getBottomSheetItemHabitat(markerDto));
            this.markerARList.add(markerAR);
            this.view.addItem(markerAR);
        }

        int height = Utils.getWindowHeight(windowManager);
        int width = Utils.getWindowWidth(windowManager);
        int radius = Math.min(width / 2, height / 2);

        this.maxY = height - radius - AUGMENTED_REALITY_MARGIN - Y_MARGIN - MAX_SIZE;
        this.minY = titleBarHeight + Y_MARGIN + MIN_SIZE;
        this.viewHorizontalAngle = view.getHorizontalViewAngle();
        this.viewVerticalAngle = view.getVerticalViewAngle();
        this.width = width;
    }

    public void animationFrame() {
        float azimuth = view.getAzimuth();
        float altitude = view.getAltitude();

        float parallax = getVerticalParallax(altitude);
        float tilt = getTilt(parallax);
        float minY = getMinY(parallax);
        float maxY = getMaxY(parallax);

        Location location = presenter.getMapView().getMyLocation();
        countBearingsAndDistances(location);

        double maxDist = getBoundaryDistances(true);
        double minDist = getBoundaryDistances(false);

        presenter.getMapView().setCameraForAugmentedRealityMode(azimuth, tilt);

        for (MarkerAR markerAR : markerARList) {
            float y = getY(markerAR.getDistance(), maxDist, minDist);
            float x = getX(markerAR.getBearing(), azimuth);

            markerAR.setIcon((String) markerAR.getMarker().getIcon().get(MarkerConstant.ICON_ACTIVE));
            markerAR.setSubtitle(markerAR.getHabitat());

            if (presenter.getActiveMarker() == markerAR.getMarker()) {
                markerAR.setAlpha(1);
                if (x < 0) {
                    x = 0;
                    markerAR.setIcon(MarkerAR.ICON_LEFT_ARROW);
                } else if (x > 1) {
                    x = 1;
                    markerAR.setIcon(MarkerAR.ICON_RIGHT_ARROW);
                }
                view.updateTitleBar(markerAR, activeMarkerDto != presenter.getActiveMarker());
                activeMarkerDto = presenter.getActiveMarker();
            } else {
                markerAR.setAlpha(getAlpha(y));
            }

            markerAR.setElevation(markerAR.getAlpha() + MIN_ELEVATION);
            markerAR.setIconSize(getSize(y));
            markerAR.setMarginLeft(getMarginLeft(x));
            markerAR.setMarginTop(getMarginTop(y, maxY, minY));
        }

        if (presenter.getActiveMarker() == null) {
            MarkerAR markerAR = getClosestMarker();
            if (markerAR != null) {
                markerAR.setSubtitle(MarkerAR.NEARLY);
                view.updateTitleBar(markerAR, activeMarkerDto != presenter.getActiveMarker());
                activeMarkerDto = presenter.getActiveMarker();
            }
        }

        updateMarkers();
    }

    private void updateMarkers() {
        for (MarkerAR markerAR : markerARList) {
            markerAR.setVisible(presenter.isAugmentedRealityMarker(markerAR.getMarker()));
            view.updateItemParams(markerAR);
        }
    }

    private float getVerticalParallax(float altitude) {
        float parallax = 2 * (90 + altitude) / viewVerticalAngle;
        parallax = parallax < -1 ? -1 : parallax > 1 ? 1 : parallax;
        return parallax;
    }

    private float getTilt(float parallax) {
        return TILT - TILT_MARGIN * parallax;
    }

    private float getMinY(float parallax) {
        if (parallax < 0)
            return minY + parallax * (minY - maxY);
        return minY;
    }

    private float getMaxY(float parallax) {
        if (parallax > 0)
            return maxY + parallax * (minY - maxY);
        return maxY;
    }

    private float getAlpha(float y) {
        return MAX_OPACITY - y * (MAX_OPACITY - MIN_OPACITY);
    }

    private float getSize(float y) {
        return (MAX_SIZE - y * (MAX_SIZE - MIN_SIZE));
    }

    private float getX(Double alpha, float bearing) {
        alpha = bearing - alpha > 180 ? alpha + 360 : alpha - bearing > 180 ? alpha - 360 : alpha;
        return (float) ((alpha - bearing) / viewHorizontalAngle + .5f);
    }

    private float getY(Double distance, double max, double min) {
        return (float) ((distance - min) / (max - min));
    }

    private int getMarginLeft(float x) {
        return (int) (width * x);
    }

    private int getMarginTop(float y, float max, float min) {
        return (int) (max - y * (max - min));
    }

    private void countBearingsAndDistances(Location location) {
        for (MarkerAR markerAR : markerARList) {
            Location loc = new Location(markerAR.getMarker().getName());
            loc.setLatitude(markerAR.getMarker().getLocation().getLatitude());
            loc.setLongitude(markerAR.getMarker().getLocation().getLongitude());

            markerAR.setDistance(location.distanceTo(loc));
            markerAR.setBearing(location.bearingTo(loc));
        }
    }

    private double getBoundaryDistances(boolean maximizing) {
        double result = -1.;
        for (MarkerAR markerAR : markerARList) {
            double distance = markerAR.getDistance();
            if (result < 0 || maximizing && distance > result || !maximizing && distance < result)
                result = distance;
        }
        return result;
    }

    private MarkerAR getClosestMarker() {
        MarkerAR closestMarkerAR = null;
        for (MarkerAR markerAR : markerARList)
            if (presenter.isAugmentedRealityMarker(markerAR.getMarker())
                    && (closestMarkerAR == null
                    || closestMarkerAR.getDistance() > markerAR.getDistance()))
                closestMarkerAR = markerAR;
        return closestMarkerAR;
    }


    public void onVoiceSearchResult(String query) {
        VoiceSearchHelper voiceSearchHelper = new VoiceSearchHelper(query, presenter.getCheckboxAllTypes(), markerARList);
        List<String> types = voiceSearchHelper.getCheckboxTypes();
        for (String type : types)
            onCheckboxTypeSelected(type, !isCheckboxActive(type));

        MarkerAR markerAR = voiceSearchHelper.getMarker();
        if (markerAR != null)
            presenter.showMarkerInfo(markerAR.getMarker().getId());
    }

    public void onWorkerStop() {
        presenter.getMapView().setCameraForNormalMode();
    }

    public void onItemClick(MarkerAR markerAR) {
        presenter.showMarkerInfo(markerAR.getMarker().getId());
    }

    public void onTitleBarClick(MarkerAR markerAR) {
        if (markerAR != null)
            presenter.showMarkerInfo(markerAR.getMarker().getId());
    }

    public void setMapForAugmentedReality() {
        if (presenter.getMode() != BasePresenter.MODE_AUGMENTED_REALITY)
            presenter.showAugmentedRealityConfirmed();
    }

    public void onCheckboxTypeSelected(String type, boolean isChecked) {
        if (isChecked)
            presenter.getCheckboxActiveTypes().add(type);
        else
            presenter.getCheckboxActiveTypes().remove(type);

        presenter.updateMarkersVisibility();
    }

    public boolean isCheckboxActive(String type) {
        return presenter.getCheckboxActiveTypes().contains(type);
    }
}
