package com.teamproject.zoo.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.CheckboxTypeDto;

import java.util.List;

public class SearchCheckboxListAdapter extends RecyclerView.Adapter<SearchCheckboxListAdapter.ViewHolder> {

    private static final String TAG = "SearchCheckboxListAdapt";

    private List<CheckboxTypeDto> items;
    private Context context;
    private SearchPresenter presenter;

    public SearchCheckboxListAdapter(List<CheckboxTypeDto> items, Context context, SearchPresenter presenter) {
        this.items = items;
        this.context = context;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG, "onCreateViewHolder: called.");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_checkbox_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        Log.d(TAG, "onBindViewHolder: called.");

        viewHolder.isChecked = items.get(i).isChecked();
        setBeltColor(viewHolder);

        viewHolder.type = items.get(i).getType();

        viewHolder.title.setText(items.get(i).getTitle());
        int id = Utils.getResourceId(items.get(i).getImage(), R.drawable.class);
        if (id > 0)
            viewHolder.image.setImageDrawable(Utils.bitmapDrawableFromVector(context, id));

        viewHolder.isChecked = presenter.isCheckboxActive(viewHolder.type);
        setBeltColor(viewHolder);

        viewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.isChecked = !presenter.isCheckboxActive(viewHolder.type);
                presenter.onCheckboxTypeClick(viewHolder.type, viewHolder.isChecked);
                setBeltColor(viewHolder);
            }
        });
    }

    private void setBeltColor(ViewHolder viewHolder) {
        if (viewHolder.isChecked) {
            viewHolder.checkBelt.setBackgroundColor(context.getColor(R.color.colorCheckboxListGreen));
        } else {
            viewHolder.checkBelt.setBackgroundColor(context.getColor(R.color.colorCheckboxListRed));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout item;
        ImageView image;
        TextView title;
        LinearLayout checkBelt;
        String type;
        boolean isChecked;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.search_list_item);
            image = itemView.findViewById(R.id.search_list_item_image);
            title = itemView.findViewById(R.id.search_list_item_title);
            checkBelt = itemView.findViewById(R.id.search_list_item_check_belt);
        }
    }
}
