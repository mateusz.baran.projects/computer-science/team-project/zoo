package com.teamproject.zoo.presenter;

import android.util.Log;

import com.teamproject.zoo.model.CheckboxTypeDto;
import com.teamproject.zoo.model.MarkerAR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class VoiceSearchHelper {

    private String query;
    private Set<CheckboxTypeDto> checkboxes;
    private List<MarkerAR> markers;
    private HashMap<String, Set<String>> mappedCheckboxWords;
    private HashMap<String, Set<String>> mappedAnimalWords;

    public VoiceSearchHelper(String query, Set<CheckboxTypeDto> checkboxes, List<MarkerAR> markers) {
        this.query = query.toLowerCase();
        this.checkboxes = checkboxes;
        this.markers = markers;
        this.mappedCheckboxWords = new HashMap<>();
        this.mappedAnimalWords = new HashMap<>();
        initMappedCheckboxWords();
        initMappedMarkerWords();
    }

    public MarkerAR getMarker() {

        for (MarkerAR markerAR : markers) {
            try {
                String title = BottomSheetLayoutHelper.getBottomSheetItemTitle(markerAR.getMarker()).toLowerCase();
                if (!title.equals("") && hasWordFromSet(query, Objects.requireNonNull(mappedAnimalWords.get(title))))
                    return markerAR;
            } catch (Exception ex) {
                Log.e("markerAR", "Error occured during voice search");
            }

        }

        return null;
    }

    public List<String> getCheckboxTypes() {
        List<String> result = new ArrayList<>();

        for (CheckboxTypeDto checkbox : checkboxes) {
            try {
                if (hasWordFromSet(query, Objects.requireNonNull(mappedCheckboxWords.get(checkbox.getTitle().toLowerCase()))))
                    result.add(checkbox.getType());
            } catch (Exception ex) {
                Log.e("markerAR", "Error occured during voice search");
            }
        }

        return result;
    }

    private boolean hasWordFromSet(String word, Set<String> set) {
        for (String string : set) {
            if (word.contains(string))
                return true;
        }
        return false;
    }

    private void initMappedCheckboxWords() {
        mappedCheckboxWords.put("ławki", new HashSet<>(Arrays.asList("ławki", "ławka", "ławek", "ławce")));
        mappedCheckboxWords.put("lody", new HashSet<>(Arrays.asList("lody", "loda", "lód")));
        mappedCheckboxWords.put("wejścia", new HashSet<>(Arrays.asList("wejście", "wejścia", "wejściach", "wejść")));
        mappedCheckboxWords.put("pawilony", new HashSet<>(Arrays.asList("pawilony", "pawilon", "strefy")));
        mappedCheckboxWords.put("restauracje", new HashSet<>(Arrays.asList("restauracje", "restauracje", "restauracja", "głód", "głodny", "posiłek", "jedzenie", "bistro", "obiad", "zjeść")));
        mappedCheckboxWords.put("toalety", new HashSet<>(Arrays.asList("toalety", "toaleta", "kibel", "wychodek", "kible", "ubikacja", "toaletę", "toalete")));
    }

    private void initMappedMarkerWords() {
        mappedAnimalWords.put("lew", new HashSet<>(Arrays.asList("lew", "lwa", "lwie", "król zwierząt", "lwy", "lwów")));
        mappedAnimalWords.put("dzieciniec", new HashSet<>(Arrays.asList("dzieciniec", "dziecińca", "dziecińcu")));
        mappedAnimalWords.put("hipopotam", new HashSet<>(Arrays.asList("hipopotam", "hipopotama", "hipopotamie", "hipopotamy", "hipopotamów")));
        mappedAnimalWords.put("jeleń", new HashSet<>(Arrays.asList("jeleń", "jelenie", "jeleni", "jeleniach", "jelenia")));
        mappedAnimalWords.put("koń", new HashSet<>(Arrays.asList("koń", "konia", "konie", "konik", "koni")));
        mappedAnimalWords.put("lama", new HashSet<>(Arrays.asList("lama", "lamy", "lamę", "lame", "lamą", "lamie", "lam")));
        mappedAnimalWords.put("lemur", new HashSet<>(Arrays.asList("lemur", "lemura", "lemury", "lemurze", "lemurów")));
        mappedAnimalWords.put("małpa", new HashSet<>(Arrays.asList("małpa", "małpy", "szympans", "szympansy", "małpie", "szympansie", "szympansów")));
        mappedAnimalWords.put("niedźwiedź", new HashSet<>(Arrays.asList("niedźwiedź", "niedźwiedzie", "niedźwiedziu", "niedźwiedziach", "niedźwiedzi")));
        mappedAnimalWords.put("nosorożec", new HashSet<>(Arrays.asList("nosorożec", "nosorożce", "nosorożcach", "nosorożcem", "nosorożców")));
        mappedAnimalWords.put("okapi", new HashSet<>(Collections.singletonList("okapi")));
        mappedAnimalWords.put("papuga", new HashSet<>(Arrays.asList("papuga", "papugi", "papugą", "papugami", "papug")));
        mappedAnimalWords.put("pingwin", new HashSet<>(Arrays.asList("pingwin", "pingwin", "pingwiny", "pingwinie", "pingwinem", "pingwinami", "pingwinów")));
        mappedAnimalWords.put("ryś", new HashSet<>(Arrays.asList("ryś", "rysie", "rysiem", "rysiami", "rysiów", "rysi")));
        mappedAnimalWords.put("zebra", new HashSet<>(Arrays.asList("zebra", "zebry", "zebrą", "zebrami", "zebrze", "zebr")));
        mappedAnimalWords.put("żyrafa", new HashSet<>(Arrays.asList("żyrafa", "żyrafy", "zyrafą", "żyrafie", "żyrafami", "żyraf")));
    }
}
