package com.teamproject.zoo.presenter;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.IBottomSheetItem;
import com.teamproject.zoo.model.MarkerType;
import com.teamproject.zoo.model.ToolbarMode;
import com.teamproject.zoo.view.BottomSheetBehaviorGoogleMapsLike;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;

public class BottomSheetPresenter {
    private static final String TAG = "BottomSheetPresenter";

    public static final int CORNER_RADIUS = 70;

    private BasePresenter presenter;
    private String customPhotoDirectory;
    private BasePresenter.IBottomSheetView view;
    private boolean isRoundCorner;

    public BottomSheetPresenter(BasePresenter presenter, BasePresenter.IBottomSheetView view) {
        this.presenter = presenter;
        this.presenter.setBottomSheetView(view);
        this.view = view;
        this.isRoundCorner = false;
    }

    public void manageBottomSheetSlideAnimation(float bottomSheetPosY, float pagerPhotoPosY, float pagerPhotoHeight, int toolbarHeight, String title) {
        float slideProgress = getSlideProgress(bottomSheetPosY, pagerPhotoPosY, pagerPhotoHeight, toolbarHeight);
        setToolbarModeAndAlphaAndColor(slideProgress);
        setCornerRadius(slideProgress);
        setToolbarTitle(slideProgress, bottomSheetPosY, toolbarHeight, title);
        setFABMyLocationVisibility(slideProgress);
        hideSearchFragment(slideProgress);
        manageFABAugmentedReality(bottomSheetPosY, slideProgress);
    }

    private float getSlideProgress(float bottomSheetPosY, float pagerPhotoPosY, float pagerPhotoHeight, int toolbarHeight) {
        float margin = (float) (toolbarHeight * .5);
        float photoOffset = bottomSheetPosY == 0 ? 0 : pagerPhotoPosY / bottomSheetPosY;
        float sheetOffset = pagerPhotoHeight == 0 ? 0 : (bottomSheetPosY - margin) / (pagerPhotoHeight - margin);

        photoOffset = photoOffset > 1 ? 1 : photoOffset < 0 ? 0 : photoOffset;
        photoOffset = (1 - photoOffset) / 2;

        sheetOffset = sheetOffset > 1 ? 1 : sheetOffset < 0 ? 0 : sheetOffset;
        sheetOffset = (1 - sheetOffset) / 2;

        return sheetOffset + photoOffset;
    }

    private void setFABMyLocationVisibility(float slideProgress) {
        if (presenter.getMode() != BasePresenter.MODE_AUGMENTED_REALITY) {
            if (slideProgress < .25)
                presenter.getMapView().showFABMyLocation();
            else
                presenter.getMapView().hideFABMyLocation();
        }
    }

    private void setToolbarModeAndAlphaAndColor(float slideProgress) {
        double alpha = Math.abs(slideProgress - .5) * 2;

        if (slideProgress > .5)
            presenter.getMainView().setToolbarColor(R.color.colorSecondaryDark);
        else
            presenter.getMainView().setToolbarColor(R.color.colorPrimary);


        if (slideProgress > .5 || presenter.getMode() != BasePresenter.MODE_AUGMENTED_REALITY)
            presenter.getMainView().setToolbarAlpha((int) (alpha * 255));
        else if (presenter.getMode() == BasePresenter.MODE_AUGMENTED_REALITY)
            presenter.getMainView().setToolbarAlpha(0);


        if (slideProgress < .25)
            presenter.resetToolbarMode();
        else if (.75 < slideProgress)
            presenter.setToolbarMode(ToolbarMode.BOTTOM_PANEL_EXPANDED_STATE_INFO);
        else
            presenter.setToolbarMode(ToolbarMode.BOTTOM_PANEL_MIDDLE_STATE_INFO);
    }

    private void setCornerRadius(float slideProgress) {
        if (slideProgress < .5 || slideProgress == .5 && isRoundCorner) {
            int radius = (int) (CORNER_RADIUS * (.5 - slideProgress) * 2);
            int photoRadius = (int) (CORNER_RADIUS * Math.pow((.5 - slideProgress) * 2, 10));
            view.setRoundedCorners(radius, photoRadius);
            isRoundCorner = slideProgress < .5;
        }
    }

    private void setToolbarTitle(float slideProgress, float bottomSheetPosY, int toolbarHeight, String title) {
        if (slideProgress > .5) {
            float offset = toolbarHeight == 0 ? 0 : bottomSheetPosY / (float) (toolbarHeight * .5);

            offset = offset > 1 ? 1 : offset < 0 ? 0 : offset;
            offset = 1 - offset;

            presenter.getMainView().setToolbarTitle((int) (offset * 255), title);
        } else {
            presenter.getMainView().setToolbarTitle(255, null);
        }
    }

    private void hideSearchFragment(float progress) {
        if (progress > 0.1)
            presenter.getSearchView().searchViewAnimator(false);
        else
            presenter.getSearchView().searchViewAnimator(true);
    }

    public void manageStateAnchorPoint() {
        view.setPagingEnabled(true);
    }

    public void manageStateCollapsed(IBottomSheetItem bottomSheetItem) {
        view.smoothScrollTo(0, 0);

        if (isNavDrawerItem(bottomSheetItem))
            view.setPagingEnabled(false);
        else
            view.setPagingEnabled(true);

        presenter.showNextMissionMarker();

        if (presenter.getMode() == BasePresenter.MODE_AUGMENTED_REALITY)
            view.hideBottomFragment();
    }

    public void manageStateExpanded(IBottomSheetItem bottomSheetItem) {
        if (isNavDrawerItem(bottomSheetItem))
            view.setPagingEnabled(true);
        else
            view.setPagingEnabled(false);
    }

    public void manageStateHidden() {
        if (presenter.isShownBottomSheet() && presenter.getMode() != BasePresenter.MODE_AUGMENTED_REALITY) {
            presenter.clearMarkerInfo();
        }
        presenter.getMainView().clearSelectionFromNavigationDrawer();
    }

    public void onItemSelected(IBottomSheetItem bottomSheetItem) {
        if (bottomSheetItem != presenter.getActiveMarker() && isMarker(bottomSheetItem)) {
            presenter.showMarker(bottomSheetItem.getId());
            presenter.getMapView().centerMarkerPosition(bottomSheetItem.getId());
        }
        boolean isScrollable = true;
        if(MarkerType.PARKING.equals(bottomSheetItem.getType()) ||
                MarkerType.BENCH.equals(bottomSheetItem.getType()) ||
                MarkerType.ICE_CREAM.equals(bottomSheetItem.getType()) ||
                MarkerType.PAVILION.equals(bottomSheetItem.getType()) ||
                MarkerType.TOILET.equals(bottomSheetItem.getType()) ||
                MarkerType.MAIN_POINT.equals(bottomSheetItem.getType()))
            isScrollable = false;
        view.setScrolling(isScrollable);
    }

    public void click() {
        if (view.isStateCollapsed())
            view.expandBottomFragment();
    }

    public void confirmBottomSheetHiding() {
        presenter.setShownBottomSheet(false);
    }

    public void confirmBottomSheetShowing() {
        presenter.setShownBottomSheet(true);
    }

    public String getCustomPhotoPath(Integer itemID) {
        File dir = new File(this.customPhotoDirectory);
        File[] files = dir.listFiles();

        if (files != null)
            for (File file : files) {
                if (file.getName().startsWith(Integer.toString(itemID))) {
                    return retrievePathFromJSON(file);
                }
            }
        return null;
    }

    private String retrievePathFromJSON(File json) {
        FileInputStream is;
        String response;
        String result = null;
        try {
            is = new FileInputStream(json);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            response = new String(buffer);

            JSONObject path = new JSONObject(response);

            result = path.get("path").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void setCustomPhotoDirectory(String customPhotoDirectory) {
        this.customPhotoDirectory = customPhotoDirectory;
    }

    public void onAugmentedRealityFABClick() {
        if (presenter.getMode() == BasePresenter.MODE_AUGMENTED_REALITY)
            presenter.hideAugmentedReality();
        else
            presenter.showAugmentedReality();
    }

    public int getShowBottomFragment(IBottomSheetItem bottomSheetItem) {
        if (presenter.getMode() == BasePresenter.MODE_AUGMENTED_REALITY || isNavDrawerItem(bottomSheetItem))
            return BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT;
        return BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED;
    }

    private void manageFABAugmentedReality(float posY, float progress) {
        if (progress == 0) {
            presenter.manageFABAugmentedReality();
            view.setBottomMarginFABAugmentedReality((int) (view.getHeight() - posY + view.getFabMargin()));
        } else {
            view.hideFABAugmentedReality();
        }
    }

    public boolean isMarker(IBottomSheetItem bottomSheetItem) {
        return MarkerType.NORMAL.equals(bottomSheetItem.getType())
                || MarkerType.MISSION.equals(bottomSheetItem.getType())
                || MarkerType.PARKING.equals(bottomSheetItem.getType());
    }

    public boolean isNavDrawerItem(IBottomSheetItem bottomSheetItem) {
        return MarkerType.TICKET.equals(bottomSheetItem.getType())
                || MarkerType.NONE.equals(bottomSheetItem.getType());
    }
}
