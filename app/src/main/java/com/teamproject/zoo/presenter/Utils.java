package com.teamproject.zoo.presenter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.lang.reflect.Field;

public class Utils {
    public static int getResourceId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Resources.NotFoundException e) {
            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int getWindowHeight(WindowManager windowManager) {
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getWindowWidth(WindowManager windowManager) {
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getStatusBarHeight(Resources resources) {
        int height = 0;

        int statusBarID = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (statusBarID > 0)
            height = (int) resources.getDimension(statusBarID);

        return height;
    }

    public static int getToolbarBarHeight(Context context) {
        TypedArray array = context.obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int toolbarHeight = array.getDimensionPixelSize(0, 0);
        array.recycle();

        return toolbarHeight;
    }

    public static BitmapDrawable bitmapDrawableFromVector(Context context, int drawableId, int height, int width, boolean dpUnits) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        assert drawable != null;
        if (dpUnits) {
            width = ((int) Utils.convertDpToPixel(width, context));
            height = ((int) Utils.convertDpToPixel(height, context));
        }
        Bitmap bitmap = bitmapFromVector(drawable, width, height);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public static BitmapDrawable bitmapDrawableFromVector(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        assert drawable != null;
        Bitmap bitmap = bitmapFromVector(drawable, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        assert drawable != null;
        return bitmapDescriptorFromVector(drawable, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int drawableId, int height, int width, boolean dpUnits) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        assert drawable != null;
        if (dpUnits) {
            width = ((int) Utils.convertDpToPixel(width, context));
            height = ((int) Utils.convertDpToPixel(height, context));
        }
        return bitmapDescriptorFromVector(drawable, width, height);
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Drawable drawable, int width, int height) {
        Bitmap bitmap = bitmapFromVector(drawable, width, height);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static Bitmap bitmapFromVector(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @Deprecated
    public static String getResNameFromFullPath(String path) {
        String[] result = path.split("\\.");
        return result.length == 0 ? path : result[result.length - 1];
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
