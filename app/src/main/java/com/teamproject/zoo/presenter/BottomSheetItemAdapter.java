package com.teamproject.zoo.presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.BabyAnimalDto;
import com.teamproject.zoo.model.BottomSheetItemConstant;
import com.teamproject.zoo.model.FeedingHoursDto;
import com.teamproject.zoo.model.IBottomSheetItem;
import com.teamproject.zoo.view.BottomSheetItemViewPager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.teamproject.zoo.presenter.BottomSheetLayoutHelper.getLayoutContents;
import static com.teamproject.zoo.presenter.BottomSheetLayoutHelper.getLayoutID;
import static com.teamproject.zoo.presenter.BottomSheetLayoutHelper.getLayoutImages;
import static com.teamproject.zoo.presenter.BottomSheetLayoutHelper.getLayouts;

public class BottomSheetItemAdapter extends PagerAdapter {

    public static final String TAG = "BottomSheetItemAdapter";

    private Context context;
    private LayoutInflater layoutInflater;
    private List<IBottomSheetItem> bottomSheetItems;
    private Map<Integer, String> bottomSheetTitles;
    private final static int ROOT_LAYOUT = R.layout.pager_item_bottom_sheet_info;
    private int mCurrentPosition;

    @SuppressLint("UseSparseArrays")
    public BottomSheetItemAdapter(Context context, List<IBottomSheetItem> bottomSheetItems) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bottomSheetItems = bottomSheetItems;
        this.bottomSheetTitles = new HashMap<>();
        this.mCurrentPosition = -1;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return bottomSheetItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LinearLayout itemView = (LinearLayout) layoutInflater.inflate(ROOT_LAYOUT, container, false);

        List<HashMap<String, Object>> layouts = getLayouts(bottomSheetItems.get(position), BottomSheetItemConstant.LAYOUTS_BOTTOM_SHEET);
        for (HashMap<String, Object> layout : layouts) {
            Map<String, String> contents = getLayoutContents(layout);
            Map<String, String> images = getLayoutImages(layout);
            String layoutResID = getLayoutID(layout);

            int layoutID = Utils.getResourceId(layoutResID, R.layout.class);
            if (layoutID > 0) {
                View childView = layoutInflater.inflate(layoutID, container, false);

                for (String contentKey : contents.keySet()) {
                    int contentID = Utils.getResourceId(contentKey, R.id.class);
                    if (contentID > 0) {
                        TextView textView = childView.findViewById(contentID);
                        String text = contents.get(contentKey);
                        if (textView != null && text != null) {
                            if (BottomSheetItemConstant.TITLE_KEY.equals(contentKey))
                                bottomSheetTitles.put(position, text);

                            textView.setText(text);
                        }
                    }
                }

                for (String imageKey : images.keySet()) {
                    int id = Utils.getResourceId(imageKey, R.id.class);
                    int imageID = Utils.getResourceId(images.get(imageKey), R.drawable.class);
                    if (id > 0 && imageID > 0) {
                        CircleImageView circleImageView = childView.findViewById(id);
                        if (circleImageView != null)
                            circleImageView.setImageResource(imageID);
                    }
                }

                itemView.addView(childView);
            }
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (position != mCurrentPosition) {
            Log.d(TAG, "position: " + position);
            View view = (View) object;
            BottomSheetItemViewPager pager = (BottomSheetItemViewPager) container;
            mCurrentPosition = position;
            pager.measureCurrentView(view);
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }

    public String getBottomSheetTitle(int position) {
        if (bottomSheetTitles.containsKey(position))
            return bottomSheetTitles.get(position);
        return "";
    }
}
