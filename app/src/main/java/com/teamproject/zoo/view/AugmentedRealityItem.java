package com.teamproject.zoo.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.MarkerAR;
import com.teamproject.zoo.presenter.AugmentedRealityPresenter;
import com.teamproject.zoo.presenter.Utils;

public class AugmentedRealityItem {

    private MarkerAR markerAR;
    private Context context;
    private ImageView imageView;
    private AugmentedRealityPresenter presenter;
    private String currentIcon;

    public AugmentedRealityItem(MarkerAR markerAR, Context context, RelativeLayout container, AugmentedRealityPresenter presenter) {
        this.markerAR = markerAR;
        this.context = context;
        this.presenter = presenter;
        this.currentIcon = "";

        this.imageView = new ImageView(context);
        this.imageView.setVisibility(View.INVISIBLE);

        container.addView(imageView);
        setupOnClickListener();
    }

    public void setupOnClickListener() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onItemClick(markerAR);
            }
        });
    }

    public void updateParams() {
        setBackground();
        imageView.setVisibility(markerAR.isVisible() ? View.VISIBLE : View.INVISIBLE);
        imageView.setAlpha(markerAR.getAlpha());
        imageView.setElevation(markerAR.getElevation());
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (markerAR.getIconSize() > 0) {
            int size = (int) Utils.convertDpToPixel(markerAR.getIconSize(), context);
            lp.height = size;
            lp.width = size;
        }

        lp.setMargins(markerAR.getMarginLeft() - lp.width / 2, markerAR.getMarginTop() - lp.height / 2, -lp.width, -lp.height);

        imageView.setLayoutParams(lp);
        imageView.requestLayout();
    }

    private void setBackground() {
        if (markerAR.getIcon() != null && !currentIcon.equals(markerAR.getIcon())) {

            currentIcon = markerAR.getIcon();
            int drawableID = Utils.getResourceId(currentIcon, R.drawable.class);

            if (drawableID < 0) {
                currentIcon = MarkerAR.ICON_DEFAULT;
                drawableID = Utils.getResourceId(MarkerAR.ICON_DEFAULT, R.drawable.class);
            }

            this.imageView.setBackground(Utils.bitmapDrawableFromVector(context, drawableID, MarkerAR.MAX_SIZE, MarkerAR.MAX_SIZE, true));
        }
    }
}
