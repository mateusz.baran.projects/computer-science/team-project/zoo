package com.teamproject.zoo.view;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;


public class CameraPreviewSurface extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder holder;
    private Camera camera;

    public CameraPreviewSurface(Context context) {
        super(context);

        this.holder = getHolder();
        this.holder.addCallback(this);

        setupCamera();
    }

    private void setupCamera() {
        camera = getCameraInstance();

        if (camera == null)
            return;

        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            camera.setDisplayOrientation(90);
        } else {
            camera.setDisplayOrientation(0);
        }
    }

    public static Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception ignored) {
        }
        return camera;
    }

    public void closeCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);

            camera.release();
            camera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (camera != null) {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            }
        } catch (IOException ignored) {
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        closeCamera();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (this.holder.getSurface() == null)
            return;

        try {

            if (camera != null)
                camera.stopPreview();
        } catch (Exception ignored) {
        }

        try {
            if (camera != null) {
                camera.setPreviewDisplay(this.holder);
                camera.startPreview();
            }
        } catch (Exception ignored) {
        }
    }

    public float getHorizontalViewAngle() {
        return camera.getParameters().getHorizontalViewAngle();
    }

    public float getVerticalViewAngle() {
        return camera.getParameters().getVerticalViewAngle();
    }
}
