package com.teamproject.zoo.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.MarkerAR;
import com.teamproject.zoo.presenter.AugmentedRealityPresenter;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class AugmentedRealityFragment extends Fragment implements BasePresenter.IAugmentedRealityView {

    public static final String TAG = "AugmentedRealityFrag";

    private static final int REQUEST_MICROPHONE = 300;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private static final int LOCATION_REQUEST_CODE = 101;

    private final AtomicBoolean running = new AtomicBoolean(false);

    private Context context;
    private View rootView;
    private RelativeLayout itemContainer;
    private AugmentedRealityPresenter presenter;
    private RelativeLayout titleBar;
    private TextView titleBarTitle;
    private TextView titleBarSubtitle;
    private ImageView titleBarImage;
    private MarkerAR activeMarkerAR;
    private ImageView titleBarVoiceSearch;

    private Toast toastMessage;

    private SpeechRecognizer speechRecognizer;
    private TextToSpeech textToSpeech;

    private CompassSensor compass;

    private CameraPreviewSurface cameraPreview;

    private Map<MarkerAR, AugmentedRealityItem> items;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.presenter = new AugmentedRealityPresenter(((MainActivity) Objects.requireNonNull(getActivity())).getBasePresenter(), this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_augmented_reality, container, false);
        setupViews();
        setTitleBar();
        setupVoiceSearch();

        return rootView;
    }

    private void setupViews() {
        itemContainer = rootView.findViewById(R.id.augmented_reality_item_container);
        titleBar = rootView.findViewById(R.id.augmented_reality_title_bar);
        titleBarTitle = rootView.findViewById(R.id.augmented_reality_title_bar_title);
        titleBarSubtitle = rootView.findViewById(R.id.augmented_reality_title_bar_subtitle);
        titleBarImage = rootView.findViewById(R.id.augmented_reality_title_bar_image);
        titleBarVoiceSearch = rootView.findViewById(R.id.augmented_reality_title_bar_voice_search_icon);
    }

    public void setTitleBar() {
        int topMargin = titleBar.getPaddingTop() + Utils.getStatusBarHeight(getResources());
        titleBar.setPadding(titleBar.getPaddingLeft(),
                topMargin,
                titleBar.getPaddingRight(),
                titleBar.getPaddingBottom());
        titleBar.setVisibility(View.VISIBLE);

        titleBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onTitleBarClick(activeMarkerAR);
            }
        });
    }

    private void setupVoiceSearch() {
        titleBarVoiceSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAudioPermissionGranted() && speechRecognizer != null && textToSpeech != null)
                    runSpeechRecognizer();
                else {
                    initializeTextToSpeech();
                    initializeSpeechRecognizer();
                }
            }
        });
    }

    @Override
    public void onResume() {
        if (cameraPreview != null) createCamera();
        if (compass != null) compass.onResume();
        if (cameraPreview != null && compass != null)
            runWorker();
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isAudioPermissionGranted()) {
            initializeSpeechRecognizer();
            initializeTextToSpeech();
        }
    }

    @Override
    public void onPause() {
        if (cameraPreview != null) cameraPreview.closeCamera();
        if (compass != null) compass.onPause();
        stopWorker();
        super.onPause();
        if (textToSpeech != null)
            textToSpeech.shutdown();
        if (speechRecognizer != null)
            speechRecognizer.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (permissions.length == 1 &&
                    permissions[0].equals(Manifest.permission.CAMERA) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                start();
            }
        }
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                start();
            }
        }
    }

    @Override
    public void start() {
        if (checkCameraPermission() && checkLocationPermission()) {
            createCamera();

            presenter.setMapForAugmentedReality();

            compass = new CompassSensor(context);
            compass.onResume();

            itemContainer.removeAllViews();
            items = new HashMap<>();
            int titleBarHeight = titleBar.getPaddingTop() + titleBar.getHeight() + titleBar.getPaddingBottom();
            presenter.setup(Objects.requireNonNull(getActivity()).getWindowManager(), titleBarHeight);

            runWorker();
        }
    }

    @Override
    public void stop() {
        if (cameraPreview != null)
            cameraPreview.closeCamera();

        stopWorker();
        compass.onPause();
        compass = null;
        cameraPreview = null;
    }

    private boolean checkCameraPermission() {
        if (checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
            return false;
        }
        return true;
    }

    public boolean checkLocationPermission() {
        if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void createCamera() {
        cameraPreview = new CameraPreviewSurface(context);
        FrameLayout preview = rootView.findViewById(R.id.camera_preview_surface);
        preview.removeAllViews();
        preview.addView(cameraPreview);
    }

    @Override
    public float getAzimuth() {
        if (compass == null)
            return -1;
        return compass.getAzimuth();
    }

    @Override
    public float getAltitude() {
        if (compass == null)
            return -1;
        return compass.getAltitude();
    }

    @Override
    public float getHorizontalViewAngle() {
        if (cameraPreview == null)
            return -1;
        return cameraPreview.getHorizontalViewAngle();
    }

    @Override
    public float getVerticalViewAngle() {
        if (cameraPreview == null)
            return -1;
        return cameraPreview.getVerticalViewAngle();
    }

    @Override
    public void addItem(MarkerAR markerAR) {
        items.put(markerAR, new AugmentedRealityItem(markerAR, context, itemContainer, presenter));
    }

    @Override
    public void updateItemParams(MarkerAR markerAR) {
        AugmentedRealityItem item = items.get(markerAR);
        if (item != null)
            item.updateParams();

    }

    @Override
    public void updateTitleBar(MarkerAR markerAR, boolean force) {
        if (markerAR != null) {
            if (force || markerAR != activeMarkerAR) {
                if (markerAR.getMarker().getImages().size() > 0) {
                    String name = markerAR.getMarker().getImages().get(0);
                    int id = Utils.getResourceId(name, R.drawable.class);
                    if (id > 0) titleBarImage.setImageResource(id);
                    else titleBarImage.setImageDrawable(null);
                } else {
                    titleBarImage.setImageDrawable(null);
                }
                titleBarTitle.setText(markerAR.getTitle());
                titleBarSubtitle.setText(markerAR.getSubtitle());
            }
        } else {
            titleBarImage.setImageDrawable(null);
            titleBarTitle.setText("");
            titleBarSubtitle.setText("");
        }
        activeMarkerAR = markerAR;
    }

    private void runWorker() {
        Thread worker = new Thread() {
            @Override
            public void run() {
                running.set(true);
                while (running.get()) {
                    try {
                        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.animationFrame();
                            }
                        });
                        Thread.sleep(AugmentedRealityPresenter.DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        presenter.onWorkerStop();
                    }
                });
            }
        };
        worker.start();
    }

    @Override
    public void stopWorker() {
        running.set(false);
    }


    // VOICE SEARCH

    public void setMicStatus(boolean isBusy) {
        if (isBusy)
            titleBarVoiceSearch.setImageResource(R.drawable.ic_mic_orange);
        else
            titleBarVoiceSearch.setImageResource(R.drawable.ic_mic_white);
    }

    private void processResult(String result) {
        presenter.onVoiceSearchResult(result);
    }

    protected void runSpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, getString(R.string.search_pl_locale));
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        speechRecognizer.startListening(intent);
    }

    private void initializeTextToSpeech() {
        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (textToSpeech.getEngines().size() == 0) {
                    showToastMessage(getString(R.string.search_error_no_engine_installed));
                } else if (status == TextToSpeech.SUCCESS) {
                    Locale locale = Locale.forLanguageTag(getString(R.string.search_pl));
                    textToSpeech.setLanguage(locale);
                }
            }
        });
    }

    private boolean isAudioPermissionGranted() {
        return ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }


    private void initializeSpeechRecognizer() {
        if (SpeechRecognizer.isRecognitionAvailable(getContext())) {

            if (!isAudioPermissionGranted()) {
                ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        REQUEST_MICROPHONE);

            } else {
                speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getContext());
                speechRecognizer.setRecognitionListener(new RecognitionListener() {
                    @Override
                    public void onReadyForSpeech(Bundle params) {
                        Log.d(TAG, "onReadyForSpeech");
                        setMicStatus(true);
                    }

                    @Override
                    public void onBeginningOfSpeech() {
                        Log.d(TAG, "onBeginningOfSpeech");
                    }

                    @Override
                    public void onRmsChanged(float rmsdB) {
                    }

                    @Override
                    public void onBufferReceived(byte[] buffer) {
                        Log.d(TAG, "onBufferReceived");
                    }

                    @Override
                    public void onEndOfSpeech() {
                        Log.d(TAG, "onEndOfSpeech");
                        setMicStatus(false);
                    }

                    @Override
                    public void onError(int error) {
                        String message;
                        switch (error) {
                            case SpeechRecognizer.ERROR_AUDIO:
                                message = getString(R.string.search_error_audio_recognition_error);
                                break;
                            case SpeechRecognizer.ERROR_CLIENT:
                                message = getString(R.string.search_error_client_side_error);
                                break;
                            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                                message = getString(R.string.search_error_insufficient_permissions);
                                break;
                            case SpeechRecognizer.ERROR_NETWORK:
                                message = getString(R.string.search_error_network_error);
                                break;
                            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                                message = getString(R.string.search_error_network_timeout);
                                break;
                            case SpeechRecognizer.ERROR_NO_MATCH:
                                message = getString(R.string.search_error_no_match);
                                break;
                            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                                message = getString(R.string.search_error_recognizer_busy);
                                break;
                            case SpeechRecognizer.ERROR_SERVER:
                                message = getString(R.string.search_error_from_server);
                                break;
                            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                                message = getString(R.string.search_error_speech_timeout);
                                break;
                            default:
                                message = getString(R.string.search_error_did_not_understand);
                                break;
                        }

                        showToastMessage(message);
                        Log.d(TAG, "onError");
                    }

                    @Override
                    public void onResults(Bundle results) {
                        List<String> words = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                        if (words != null) {
                            processResult(words.get(0));
                        } else {
                            showToastMessage(getString(R.string.search_error_something_goes_wrong));
                        }
                        Log.d(TAG, "onResults");
                    }

                    @Override
                    public void onPartialResults(Bundle partialResults) {
                        Log.d(TAG, "onPartialResults");
                    }

                    @Override
                    public void onEvent(int eventType, Bundle params) {
                        Log.d(TAG, "onEvent");
                    }
                });
            }
        } else {
            showToastMessage(getString(R.string.search_error_not_available));
        }
    }

    private void showToastMessage(String message) {
        if (toastMessage != null)
            toastMessage.cancel();

        toastMessage = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
        toastMessage.show();
    }
}
