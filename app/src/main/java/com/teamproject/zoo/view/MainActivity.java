package com.teamproject.zoo.view;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.teamproject.zoo.R;
import com.teamproject.zoo.model.HomeIconType;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.MainPresenter;
import com.teamproject.zoo.presenter.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class MainActivity extends AppCompatActivity implements BasePresenter.IMainView {

    private BasePresenter basePresenter;
    private MainPresenter presenter;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private ActionBar actionBar;
    private Toolbar toolbar;
    private Integer toolbarGroupID;
    private Integer homeIconType;
    private Map<Integer, Map<Integer, Integer>> homeIconIDAnimations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(getApplicationContext());

        setupNavigationDrawer();
        setupToolbar();
        setupStatusBar();
        setTheme(R.style.AppTheme);

        basePresenter = new BasePresenter();
        presenter = new MainPresenter(basePresenter, this);

        addFragments();
    }

    private void setupStatusBar() {
        int statusBarHeight = Utils.getStatusBarHeight(getResources());
        toolbar.setPadding(0, statusBarHeight, 0, 0);
        navigationView.setPadding(0, statusBarHeight, 0, 0);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getColor(R.color.colorStatusBar));
    }

    private void addFragments() {
        addFragment(R.id.map_container, new MapFragment(), getString(R.string.fragment_tag_map));
        addFragment(R.id.bottom_sheet_container, new BottomSheetFragment(), getString(R.string.fragment_tag_bottom_sheet));
        addFragment(R.id.route_selector_container, new RouteSelectorFragment(), getString(R.string.fragment_tag_route_selector));
        addFragment(R.id.augmented_reality_container, new AugmentedRealityFragment(), getString(R.string.fragment_tag_augmented_reality));
        addFragment(R.id.search_container, new SearchFragment(), getString(R.string.fragment_tag_search));
        addFragment(R.id.find_car_container, new FindCarFragment(), getString(R.string.fragment_tag_find_car));
    }

    private void addFragment(int placeholderId, Fragment fragment, String fragmentTag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(placeholderId, fragment, fragmentTag);
        ft.commitAllowingStateLoss();
    }

    public BasePresenter getBasePresenter() {
        return basePresenter;
    }

    @Override
    public void fadeSplashScreen() {
        AlphaAnimation animation = new AlphaAnimation(1f, 0f);
        animation.setDuration(300);
        animation.setFillAfter(true);
        findViewById(R.id.splash_screen_layout).startAnimation(animation);
    }

    @Override
    public void clearSelectionFromNavigationDrawer() {
        if (navigationView.getCheckedItem() != null)
            navigationView.getCheckedItem().setChecked(false);
    }

    @Override
    public void setToolbarNormal() {
        changeToolbarGroup(R.id.menu_main_group);
        changeToolbarHomeIcon(HomeIconType.HAMBURGER);
        setToolbarTitle(0, "");
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void setToolbarRouteSelection() {
        changeToolbarGroup(R.id.menu_route_selector_group);
        changeToolbarHomeIcon(HomeIconType.BACK_ARROW);
        setToolbarTitle(255, getString(R.string.menu_title_show_routes));
        toolbar.getMenu().findItem(R.id.menu_cancel_route_button).setVisible(presenter.isSelectedRoute());
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarBottomPanelExpandedStateInfo() {
        changeToolbarGroup(R.id.menu_bottom_panel_expanded_info_group);
        changeToolbarHomeIcon(HomeIconType.BACK_ARROW);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarBottomPanelMiddleStateInfo() {
        changeToolbarGroup(R.id.menu_bottom_panel_expanded_info_group);
        changeToolbarHomeIcon(HomeIconType.BACK_ARROW);
        setToolbarTitle(0, "");
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarParking() {
        changeToolbarGroup(R.id.menu_parking_group);
        changeToolbarHomeIcon(HomeIconType.BACK_ARROW);
        setToolbarTitle(255, getString(R.string.menu_title_parking));
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarAugmentedReality() {
        changeToolbarGroup(R.id.menu_augmented_reality_group);
        toolbar.setVisibility(View.INVISIBLE);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarSearch() {
        changeToolbarGroup(R.id.menu_search_group);
        changeToolbarHomeIcon(HomeIconType.BACK_ARROW);
        setToolbarTitle(0, getString(R.string.menu_item_title_search));
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarFindCar() {
        changeToolbarGroup(R.id.menu_find_car_group);
        changeToolbarHomeIcon(HomeIconType.BACK_ARROW);
        setToolbarTitle(255, getString(R.string.find_car_toolbar_title));
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void setToolbarAlpha(int alpha) {
        toolbar.getBackground().setAlpha(alpha);
    }

    @Override
    public void setToolbarColor(int color) {
        toolbar.setBackgroundColor(getColor(color));
    }

    @Override
    public void setToolbarTitle(int alpha, String title) {
        if (title != null)
            toolbar.setTitle(title);
        int color = ColorUtils.setAlphaComponent(getColor(R.color.colorText), alpha);
        toolbar.setTitleTextColor(color);
    }

    @Override
    public void openNavigationDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void closeNavigationDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean isOpenNavigationDrawer() {
        return drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    private void changeToolbarGroup(int groupID) {
        if (toolbarGroupID != groupID) {
            toolbar.setVisibility(View.VISIBLE);
            toolbar.getMenu().setGroupVisible(toolbarGroupID, false);
            toolbar.getMenu().setGroupVisible(groupID, true);
            this.toolbarGroupID = groupID;
        }
    }

    private void changeToolbarHomeIcon(int iconType) {
        Integer iconID = Objects.requireNonNull(homeIconIDAnimations.get(homeIconType)).get(iconType);
        if (iconID != null) {
            AnimatedVectorDrawable drawable = (AnimatedVectorDrawable) getDrawable(iconID);
            actionBar.setHomeAsUpIndicator(drawable);
            assert  drawable != null;
            drawable.start();
            homeIconType = iconType;
        }
    }

    @SuppressLint("UseSparseArrays")
    private void setupToolbar() {
        Map<Integer, Integer> hamburger = new HashMap<>();
        hamburger.put(HomeIconType.BACK_ARROW, R.drawable.ic_menu_anim_hamburger_to_back);

        Map<Integer, Integer> backArrow = new HashMap<>();
        backArrow.put(HomeIconType.HAMBURGER, R.drawable.ic_menu_anim_back_to_hamburger);

        homeIconIDAnimations = new HashMap<>();
        homeIconIDAnimations.put(HomeIconType.HAMBURGER, hamburger);
        homeIconIDAnimations.put(HomeIconType.BACK_ARROW, backArrow);

        toolbarGroupID = 0;
        homeIconType = HomeIconType.HAMBURGER;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_hamburger_button);
        setToolbarNormal();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                presenter.onMenuHome();
                return true;
            case R.id.menu_show_routes_button:
                presenter.onMenuShowRouteSelector();
                return true;
            case R.id.menu_cancel_route_button:
                presenter.onMenuClearSelectedRoute();
                return true;
            case R.id.menu_search_button:
                presenter.onMenuSearch();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        try {
            if (presenter.onBackArrow())
                moveTaskToBack(true);
        } catch (Exception ex) {
            super.onBackPressed();
        }
    }

    private void setupNavigationDrawer() {
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        setNavigationDrawerListener();

        Menu menu = navigationView.getMenu();

        //setting navDrawer headers style
        MenuItem[] menuHeaders = new MenuItem[]{
                menu.findItem(R.id.nav_header1),
                menu.findItem(R.id.nav_header2),
                menu.findItem(R.id.nav_header3)};

        for (MenuItem i : menuHeaders) {
            SpannableString s = new SpannableString(i.getTitle());
            s.setSpan(new TextAppearanceSpan(this, R.style.NavDrawerHeader), 0, s.length(), 0);
            i.setTitle(s);
        }
    }

    private void setNavigationDrawerListener() {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {
                            case R.id.nav_info:
                                presenter.onNavigationDrawerInfo();
                                break;
                            case R.id.nav_tickets:
                                presenter.onNavigationDrawerTickets();
                                break;
                            case R.id.nav_open_hours:
                                presenter.onNavigationDrawerOpeningHours();
                                break;
                            case R.id.nav_parkings:
                                presenter.onNavigationDrawerShowParking();
                                break;
                            case R.id.nav_find_car:
                                presenter.onNavigationDrawerFindCar();
                                break;
                            case R.id.nav_missions_list:
                                presenter.onNavigationDrawerMissionList();
                                break;
                            case R.id.nav_small_animals:
                                presenter.onNavigationDrawerBabyAnimals();
                                break;
                            case R.id.nav_feeding:
                                presenter.onNavigationDrawerFeeding();
                                break;
                        }
                        return true;
                    }
                }
        );
    }
}
