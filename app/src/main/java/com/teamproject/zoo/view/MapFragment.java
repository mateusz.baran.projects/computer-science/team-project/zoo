package com.teamproject.zoo.view;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.firestore.GeoPoint;
import com.teamproject.zoo.R;
import com.teamproject.zoo.model.MarkerConstant;
import com.teamproject.zoo.model.MarkerDto;
import com.teamproject.zoo.model.RouteDto;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.MapPresenter;
import com.teamproject.zoo.presenter.Utils;

import java.util.HashMap;
import java.util.Objects;

import static android.content.Context.LOCATION_SERVICE;
import static android.support.v4.content.ContextCompat.checkSelfPermission;

public class MapFragment extends Fragment implements BasePresenter.IMapView, GoogleMap.OnCameraMoveListener,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, LocationListener, LocationSource {

    public static final int AUGMENTED_REALITY_MARGIN = 100;

    private static final int DURATION = 2000;
    private static final long UPDATE_INTERVAL = 2000;
    private static final float MIN_DISTANCE = 5;

    private static final int LOCATION_REQUEST_CODE = 101;

    private Location myLocation;
    private OnLocationChangedListener locationListener;
    private LocationManager locationManager;
    private ImageView fabMapMyLocation;

    private Context context;
    private MapPresenter presenter;

    private GoogleMap googleMap;
    private LatLngBounds zooMapBounds;
    private HashMap<Integer, Marker> markers;
    private Polyline route;
    private Marker savedCarMarker;

    private Boolean isCameraAnimationDone;
    private Boolean isAnimationDone;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.presenter = new MapPresenter(((MainActivity) Objects.requireNonNull(getActivity())).getBasePresenter(), this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onPause() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        setUpMapIfNeeded();
        super.onResume();
    }

    private void setUpMapIfNeeded() {
        if (googleMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

            assert mapFragment != null;
            mapFragment.getMapAsync(this);
        } else if (locationManager == null && checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setupLocationPermission();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        googleMap.setOnMapLoadedCallback(this);

        setupMapBackground();
        setupMapElements();
        setupMapBounds();
        setupDefaultMapLocation();
        setupMapGroundOverlays();
        setupMapListeners();
        setupNormalMapPreferences();
        setupMyLocation();
    }

    public boolean checkLocationPermission() {
        if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    public void setupLocationPermission() {
        if (checkLocationPermission()) {
            googleMap.setMyLocationEnabled(true);
            googleMap.setLocationSource(this);

            fabMapMyLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    centerMyLocation();
                }
            });

            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            if (locationManager != null) {
                boolean gpsIsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean networkIsEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (gpsIsEnabled)
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL, MIN_DISTANCE, this);
                else if (networkIsEnabled)
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, UPDATE_INTERVAL, MIN_DISTANCE, this);
            }
        } else {
            fabMapMyLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupLocationPermission();
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupLocationPermission();
            }
        }
    }

    private void setupMyLocation() {
        myLocation = new Location("default");
        myLocation.setLongitude(17.074494);
        myLocation.setLatitude(51.105134);

        fabMapMyLocation = Objects.requireNonNull(getActivity()).findViewById(R.id.fab_map_my_location);
        fabMapMyLocation.setVisibility(View.VISIBLE);

        setupLocationPermission();
    }

    private void centerMyLocation() {
        if (myLocation != null) {
            LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public Location getMyLocation() {
        return myLocation;
    }

    @Override
    public boolean isAnimationDone() {
        return isAnimationDone == null || isAnimationDone;
    }

    @Override
    public void deltaHeight(final int delta) {
        final int height = Utils.getWindowHeight(Objects.requireNonNull(getActivity()).getWindowManager());
        final LinearLayout mapView = Objects.requireNonNull(getActivity()).findViewById(R.id.map_container);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mapView.getLayoutParams();
        params.height = height - delta;
        mapView.setLayoutParams(params);
        mapView.requestLayout();
        mapView.refreshDrawableState();
    }

    private void setupMapListeners() {
        googleMap.setOnCameraMoveListener(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapClickListener(this);
    }

    private void setupMapBackground() {
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_zoo_style));
    }

    private void setupMapBounds() {
        LatLng southWestCorner = new LatLng(
                presenter.getZooMap().SOUTH_WEST_MAP_CORNER_LAT,
                presenter.getZooMap().SOUTH_WEST_MAP_CORNER_LNG);
        LatLng northEastCorner = new LatLng(
                presenter.getZooMap().NORTH_EAST_MAP_CORNER_LAT,
                presenter.getZooMap().NORTH_EAST_MAP_CORNER_LNG);

        zooMapBounds = new LatLngBounds(southWestCorner, northEastCorner);
    }

    private void setupDefaultMapLocation() {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(zooMapBounds.getCenter())
                .zoom(presenter.getZooMap().MAP_ZOOM_ZONE_MIN)
                .bearing(presenter.getZooMap().MAP_BEARING)
                .build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void setupMapGroundOverlays() {
        LatLng pos = new LatLng(presenter.getZooMap().CENTER_MAP_POINT_LAT,
                presenter.getZooMap().CENTER_MAP_POINT_LNG);

        GroundOverlayOptions zooMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromBitmap(Utils.decodeSampledBitmapFromResource(getResources(), R.drawable.map_zoo, 4096, 2048)))
                .position(pos, presenter.getZooMap().MAP_WIDTH)
                .bearing(presenter.getZooMap().MAP_BEARING)
                .zIndex(1);

        googleMap.addGroundOverlay(zooMap);
    }

    private void setupAugmentedRealityMapPreferences() {
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);

        googleMap.setLatLngBoundsForCameraTarget(null);
    }

    private void setupNormalMapPreferences() {
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        googleMap.setMinZoomPreference(presenter.getZooMap().MAP_ZOOM_ZONE_MIN);
        googleMap.setMaxZoomPreference(presenter.getZooMap().MAP_ZOOM_ZONE_MAX);

        googleMap.setLatLngBoundsForCameraTarget(zooMapBounds);
    }

    private void setupParkingPreferences() {
        googleMap.setMinZoomPreference(12);
        googleMap.setMaxZoomPreference(presenter.getZooMap().MAP_ZOOM_ZONE_MAX);

        googleMap.setLatLngBoundsForCameraTarget(null);
    }

    private void setupFindCarPreferences() {
        googleMap.setMinZoomPreference(12);
        googleMap.setMaxZoomPreference(presenter.getZooMap().MAP_ZOOM_ZONE_MAX);

        googleMap.setLatLngBoundsForCameraTarget(null);
    }

    public void setCameraForParkingMode() {
        setupParkingPreferences();

        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_parking_style));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(zooMapBounds.getCenter())
                .zoom(12)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void setCameraForFindCarMode() {
        setupFindCarPreferences();

        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_parking_style));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(zooMapBounds.getCenter())
                .zoom(12)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void setCameraForNormalMode() {
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_zoo_style));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(zooMapBounds.getCenter())
                .zoom(presenter.getZooMap().MAP_ZOOM_ZONE_MIN)
                .bearing(presenter.getZooMap().MAP_BEARING)
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                setupNormalMapPreferences();
            }

            @Override
            public void onCancel() {
                setupNormalMapPreferences();
            }
        });
    }

    @Override
    public void setCameraForAugmentedRealityMode(float bearing, float tilt) {
        setupAugmentedRealityMapPreferences();

        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_zoo_style));
        LatLng target = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(target)
                .tilt(tilt)
                .zoom(18)
                .bearing(bearing)
                .build();

        if (isCameraAnimationDone == null) {
            isCameraAnimationDone = false;
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), DURATION, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    isCameraAnimationDone = true;
                }

                @Override
                public void onCancel() {
                    isCameraAnimationDone = true;
                }
            });
        } else if (isCameraAnimationDone) {
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @SuppressLint("UseSparseArrays")
    private void setupMapElements() {
        markers = new HashMap<>();

        presenter.initMapElements();
        presenter.onZoomChangeEvent(presenter.getZooMap().MAP_ZOOM_ZONE_MIN);
    }

    @Override
    public void onMapLoaded() {
        presenter.onMapLoaded();
    }

    @Override
    public void onCameraMove() {
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        presenter.onZoomChangeEvent(cameraPosition.zoom);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        presenter.onMarkerClick((Integer) marker.getTag());
        return true;
    }

    @Override
    public void onMapClick(LatLng arg0) {
        presenter.onMapClick();
    }

    @Override
    public void addMarker(MarkerDto markerDto) {
        LatLng position = new LatLng(markerDto.getLocation().getLatitude(), markerDto.getLocation().getLongitude());

        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(position)
                .visible(false)
                .title(markerDto.getName()));

        marker.setTag(markerDto.getId());

        markers.put(markerDto.getId(), marker);
        deactivateMarker(markerDto);
    }

    @Override
    public void addCarMarker(MarkerOptions markerOptions) {
        if (markerOptions != null)
            savedCarMarker = googleMap.addMarker(markerOptions);
    }

    @Override
    public void removeCarMarker() {
        if (savedCarMarker != null)
            savedCarMarker.remove();
    }

    @Override
    public void setMarkerVisibility(Integer id, boolean isVisible) {
        Marker marker = markers.get(id);
        if (marker != null && marker.isVisible() != isVisible) marker.setVisible(isVisible);
    }

    @Override
    public void activateMarker(MarkerDto markerDto) {
        Marker marker = markers.get(markerDto.getId());
        int id = Utils.getResourceId((String) markerDto.getIcon().get(MarkerConstant.ICON_ACTIVE), R.drawable.class);
        id = id > 0 ? id : Utils.getResourceId(MarkerConstant.DEFAULT_MARKER, R.drawable.class);

        if (id > 0 && marker != null)
            marker.setIcon(Utils.bitmapDescriptorFromVector(getContext(), id,
                    MarkerConstant.ICON_ACTIVE_SIZE, MarkerConstant.ICON_ACTIVE_SIZE, true));
    }

    @Override
    public void deactivateMarker(MarkerDto markerDto) {
        Marker marker = markers.get(markerDto.getId());
        int id = Utils.getResourceId((String) markerDto.getIcon().get(MarkerConstant.ICON_INACTIVE), R.drawable.class);
        id = id > 0 ? id : Utils.getResourceId(MarkerConstant.DEFAULT_MARKER, R.drawable.class);

        if (id > 0 && marker != null)
            marker.setIcon(Utils.bitmapDescriptorFromVector(getContext(), id,
                    MarkerConstant.ICON_INACTIVE_SIZE, MarkerConstant.ICON_INACTIVE_SIZE, true));
    }

    @Override
    public void centerMarkerPosition(Integer id) {
        Marker marker = markers.get(id);

        if (marker != null)
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
    }

    @Override
    public void showMarkerWindow(Integer id) {
        Marker marker = markers.get(id);

        if (marker != null) {
            marker.showInfoWindow();
        }
    }

    @Override
    public void setRoutePolyline(RouteDto routeDto) {
        clearRoutePolyline();

        PolylineOptions polylineOptions = new PolylineOptions();
        for (GeoPoint geoPoint : routeDto.getPoints()) {
            LatLng point = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
            polylineOptions.add(point);
        }

        polylineOptions
                .zIndex(2)
                .jointType(JointType.ROUND)
                .color(context.getColor(R.color.colorRoute))
                .geodesic(true);

        route = googleMap.addPolyline(polylineOptions);
    }

    @Override
    public void clearRoutePolyline() {
        if (route != null) {
            route.remove();
            route = null;
        }
    }

    @Override
    public void showFABMyLocation() {
        ((FloatingActionButton) fabMapMyLocation).show();
    }

    @Override
    public void hideFABMyLocation() {
        ((FloatingActionButton) fabMapMyLocation).hide();
    }

    @Override
    public void setTopMarginFABMyLocation(int margin) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) fabMapMyLocation.getLayoutParams();
        lp.topMargin = margin;
        fabMapMyLocation.setLayoutParams(lp);
        fabMapMyLocation.requestLayout();
    }

    @Override
    public void setAugmentedRealityMapMode(final boolean enable) {

        isAnimationDone = false;
        isCameraAnimationDone = null;

        final LinearLayout mapView = Objects.requireNonNull(getActivity()).findViewById(R.id.map_container);
        mapView.setClipToOutline(true);

        final int height = Utils.getWindowHeight(getActivity().getWindowManager());
        final int width = Utils.getWindowWidth(getActivity().getWindowManager());
        final int radius = Math.min(width / 2, height / 2);

        ValueAnimator anim = enable ? ValueAnimator.ofFloat(0, 1) : ValueAnimator.ofFloat(1, 0);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float progress = (float) valueAnimator.getAnimatedValue();

                if (progress > .5) hideFABMyLocation();
                else showFABMyLocation();

                presenter.augmentedRealityAnimationProgress(progress);

                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius((int) (radius * progress));
                mapView.setBackground(shape);

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mapView.getLayoutParams();
                params.height = (int) (height - progress * (height - radius * 2 - AUGMENTED_REALITY_MARGIN));
                params.topMargin = (int) ((height - radius - AUGMENTED_REALITY_MARGIN) * progress);
                mapView.setLayoutParams(params);
                mapView.requestLayout();
            }
        });

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!enable) presenter.augmentedRealityFinished();
                isAnimationDone = true;
            }
        });

        anim.setDuration(DURATION);
        anim.start();
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        locationListener = onLocationChangedListener;
        locationListener.onLocationChanged(myLocation);
    }

    @Override
    public void deactivate() {
        locationListener = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
        locationListener.onLocationChanged(myLocation);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
