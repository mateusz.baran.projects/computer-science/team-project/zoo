package com.teamproject.zoo.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.RouteDto;

public class RouteSelectorItemFragment extends Fragment {

    // Store instance variables
    private String title, description, time, length;
    private boolean isRouteOn;
    private int routeID;
    View view;

    public static RouteSelectorItemFragment newInstance(RouteDto route, String time, String length) {
        RouteSelectorItemFragment fragmentFirst = new RouteSelectorItemFragment();
        Bundle args = new Bundle();
        args.putInt("routeID", route.getId());
        args.putString("title", route.getName());
        args.putString("description", route.getDescription());
        args.putString("time", Float.toString((float)route.getTime()/60));
        args.putString("length", Integer.toString(route.getDistance()));
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            routeID = getArguments().getInt("page", 0);
            title = getArguments().getString("title", "");
            description = getArguments().getString("description", "");
            time = getArguments().getString("time", "0") + " h";
            length = getArguments().getString("length", "0") + " km";
            isRouteOn = false;
        }
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pager_item_route_selector, container, false);
        fillFragment();
        return view;
    }

    private void fillFragment() {
        TextView tv_title = view.findViewById(R.id.textView_route_title);
        TextView tv_desc = view.findViewById(R.id.textView_route_description);
        TextView tv_time = view.findViewById(R.id.textView_route_time);
        TextView tv_length = view.findViewById(R.id.textView_route_length);



        tv_title.setText(title);
        tv_desc.setText(description);
        tv_desc.setMovementMethod(new ScrollingMovementMethod());
        tv_time.setText(time);
        tv_length.setText(length);
    }
}
