package com.teamproject.zoo.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.teamproject.zoo.R;
import com.teamproject.zoo.model.MarkerConstant;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.FindCarPresenter;
import com.teamproject.zoo.presenter.Utils;

import java.util.Objects;

public class FindCarFragment extends Fragment implements BasePresenter.IFindCarView {

    private FindCarPresenter presenter;
    private View rootView;
    private ConstraintLayout findCarLayout;
    private LinearLayout navigateToCarLinearLayout, saveLocationLinearLayout;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.presenter = new FindCarPresenter(((MainActivity) Objects.requireNonNull(getActivity())).getBasePresenter(), this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_find_car, container, false);
        setupViews();
        setupListeners();

        return rootView;
    }

    @Override
    public void showFindCarPanel() {
        Animation bottomUp = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_up);
        findCarLayout.startAnimation(bottomUp);
        findCarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFindCarPanel() {
        Animation bottomDown = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_down);
        findCarLayout.startAnimation(bottomDown);
        findCarLayout.setVisibility(View.GONE);
    }

    private void setupViews() {
        findCarLayout = rootView.findViewById(R.id.find_car_constraint_layout);
        navigateToCarLinearLayout = rootView.findViewById(R.id.navigate_to_car_linear_layout);
        saveLocationLinearLayout = rootView.findViewById(R.id.save_location_linear_layout);
    }

    private void setupListeners() {
        navigateToCarLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng location = getLocationFromPreferences();
                if (location != null) {
                   openNavigationToLocation(location);
                } else {
                    showToastAlert(getString(R.string.find_car_none_location_saved));
                }

            }
        });

        saveLocationLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mock
                double latitude = 51.107883;
                double longitude = 17.038538;
                LatLng location = new LatLng(latitude, longitude);

                saveLocation(location);
            }
        });
    }

    public void openNavigationToSavedLocation() {
        LatLng location = getLocationFromPreferences();
        if (location != null)
            openNavigationToLocation(location);
        else
            showToastAlert(getString(R.string.find_car_none_location_saved));
    }

    public void openNavigationToLocation(LatLng location) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + location.latitude + "," + location.longitude));
        Objects.requireNonNull(getContext()).startActivity(intent);
    }

    public MarkerOptions getSavedCarMarker(){
        LatLng savedLocation = getLocationFromPreferences();

        if (savedLocation == null)
            return null;

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(savedLocation);
        int id = Utils.getResourceId("marker_parking", R.drawable.class);
        id = id > 0 ? id : Utils.getResourceId(MarkerConstant.DEFAULT_MARKER, R.drawable.class);
        markerOptions.icon(Utils.bitmapDescriptorFromVector(getContext(), id,
                MarkerConstant.ICON_ACTIVE_SIZE, MarkerConstant.ICON_ACTIVE_SIZE, true));
        markerOptions.title(getString(R.string.find_car_car));

        return markerOptions;
    }

    private void saveLocation(final LatLng location) {
        LatLng savedLocation = getLocationFromPreferences();
        if (savedLocation != null) {
            saveLocationWithConfirmation(location);
        } else {
            saveLocationToPreferences(location);
            showToastAlert(getString(R.string.find_car_location_saved_successfull));
        }

    }

    private LatLng getLocationFromPreferences() {
        String savedLocation = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getString(R.string.find_car_shared_prefs_tag), "");
        if (savedLocation == null || savedLocation.equals("")) {
            return null;
        }
        double lat = Double.parseDouble(savedLocation.split(",")[0]);
        double lng = Double.parseDouble(savedLocation.split(",")[1]);
        return new LatLng(lat, lng);
    }

    private void saveLocationToPreferences(LatLng location) {
        String locToSave = location.latitude + "," + location.longitude;
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString(getString(R.string.find_car_shared_prefs_tag), locToSave).apply();
    }

    private void saveLocationWithConfirmation(final LatLng location) {
        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                .setIcon(R.drawable.ic_warning_alert)
                .setTitle(R.string.find_car_car_location)
                .setMessage(R.string.find_car_location_confirm)
                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveLocationToPreferences(location);
                        showToastAlert(getString(R.string.find_car_location_saved_successfull));
                    }

                })
                .setNegativeButton(R.string.find_car_no, null)
                .show();
    }

    private void showToastAlert(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
