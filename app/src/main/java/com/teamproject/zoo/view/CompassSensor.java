package com.teamproject.zoo.view;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import static android.content.Context.SENSOR_SERVICE;

public class CompassSensor implements SensorEventListener {

    private SensorManager sensorManager;
    private float altitude;
    private float azimuth;
    private float roll;


    public CompassSensor(Context context) {
        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        azimuth = 0;
    }


    public void onResume() {
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    public void onPause() {
        sensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d("CompassSensor", (int) event.values[0] + " " + (int) event.values[1] + " " + (int) event.values[2]);
        azimuth = exponentialSmoothing360(event.values[0], azimuth);
        altitude = exponentialSmoothing180(event.values[1], altitude);
        roll = exponentialSmoothing180(event.values[2], roll);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private float exponentialSmoothing360(float input, float output) {
        output = input - output > 180 ? output + 360 : output - input > 180 ? output - 360 : output;
        return (output + .2f * (input - output) + 360) % 360;
    }

    private float exponentialSmoothing180(float input, float output) {
        output = input - output > 180 ? output + 360 : output - input > 180 ? output - 360 : output;
        return (output + .2f * (input - output) + 540) % 360 - 180;
    }

    public float getAzimuth() {
        return azimuth;
    }

    public float getAltitude() {
        return altitude;
    }

    public float getRoll() {
        return roll;
    }
}
