package com.teamproject.zoo.view;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.IBottomSheetItem;
import com.teamproject.zoo.model.MarkerType;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.BottomSheetItemAdapter;
import com.teamproject.zoo.presenter.BottomSheetItemPhotoAdapter;
import com.teamproject.zoo.presenter.BottomSheetPresenter;
import com.teamproject.zoo.presenter.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BottomSheetFragment extends Fragment implements BasePresenter.IBottomSheetView {

    public static final long MEDIUM_HIDE_DURATION = 400;
    public static final long LONG_HIDE_DURATION = 500;

    public static final String TAG = "BottomSheetFragment";
    public static final String MISSION_PHOTO_PATHS = "mission_photo_paths";
    public static final String MARKER_ID = "markerID";

    public static final int OFFSCREEN_PAGE_LIMIT = 10;
    public static final int MINIMUM_VELOCITY_FACTOR = 10;

    private BottomSheetItemViewPager infoViewPager;
    private CustomViewPager photoViewPager;
    private BottomSheetBehaviorGoogleMapsLike behavior;
    private CustomNestedScrollView bottomSheet;
    private View view;
    private FloatingActionButton floatingActionButton;
    private FloatingActionButton fabAugmentedReality;
    private List<IBottomSheetItem> bottomSheetItems;
    private int position;
    private BottomSheetPresenter presenter;
    private int toolbarHeight;
    private int height;
    private int fabMargin;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.presenter = new BottomSheetPresenter(((MainActivity) Objects.requireNonNull(getActivity())).getBasePresenter(), this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        initReferences();
        setupListeners();

        this.bottomSheet.setPresenter(presenter);
        setHasOptionsMenu(true);

        initToolbarHeight();
        initBehavior();
        initFloatingActionButton();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideBottomFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bottomSheetItems != null && 0 <= position && position < bottomSheetItems.size()) {
            updateViewPagers(position);
        }
    }


    private void initToolbarHeight() {
        if (getContext() != null)
            toolbarHeight = Utils.getToolbarBarHeight(getContext()) + Utils.getStatusBarHeight(getResources());
        else
            toolbarHeight = 0;

        height = Utils.getWindowHeight(Objects.requireNonNull(getActivity()).getWindowManager());
        fabMargin = (int) getResources().getDimension(R.dimen.fab_margin);
    }

    private void initBehavior() {
        behavior = BottomSheetBehaviorGoogleMapsLike.from(bottomSheet);
//        behavior.setContext(getContext());
//        behavior.setPeekHeightDimen(R.dimen.bottom_sheet_peek_height);
        behavior.increaseMinimumVelocity(MINIMUM_VELOCITY_FACTOR);
        behavior.addBottomSheetCallback(new BottomSheetBehaviorGoogleMapsLike.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.d(TAG, "onStateChanged: " + newState);

                switch (newState) {
                    case BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED:
                        presenter.manageStateCollapsed(bottomSheetItems.get(position));
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_DRAGGING:
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED:
                        presenter.manageStateExpanded(bottomSheetItems.get(position));
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT:
                        presenter.manageStateAnchorPoint();
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN:
                        presenter.manageStateHidden();
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_SETTLING:
                        break;
                }

                manageBottomSheetSlideAnimation(bottomSheet.getY());
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                manageBottomSheetSlideAnimation(bottomSheet.getY());
            }
        });
    }

    private void manageBottomSheetSlideAnimation(float bottomSheetPosY) {
        BottomSheetItemAdapter adapter = (BottomSheetItemAdapter) infoViewPager.getAdapter();
        String title = adapter == null ? "" : adapter.getBottomSheetTitle(infoViewPager.getCurrentItem());
        presenter.manageBottomSheetSlideAnimation(bottomSheetPosY, photoViewPager.getY(), photoViewPager.getHeight(), toolbarHeight, title);
    }

    public void updateViewPagers(int position) {
        this.position = position;

        Parcel parcel = Parcel.obtain();
        parcel.writeParcelable(Preference.BaseSavedState.EMPTY_STATE, 0);

        parcel.writeInt(position);
        parcel.writeParcelable(null, 0);

        parcel.setDataPosition(0);
        ViewPager.SavedState savedState = ViewPager.SavedState.CREATOR.createFromParcel(parcel);


        BottomSheetItemAdapter bottomSheetItemAdapter = new BottomSheetItemAdapter(getContext(), bottomSheetItems);

        infoViewPager.measureCurrentView(null);
        infoViewPager.setAdapter(null);
        infoViewPager.setWindowManager(Objects.requireNonNull(getActivity()).getWindowManager());
        infoViewPager.onRestoreInstanceState(savedState);
        infoViewPager.setAdapter(bottomSheetItemAdapter);
        infoViewPager.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT);

        BottomSheetItemPhotoAdapter bottomSheetItemPhotoAdapter = new BottomSheetItemPhotoAdapter(getContext(),
                extractMarkersPhotos(this.bottomSheetItems), extractMarkersCustomPhotos(bottomSheetItems));

        photoViewPager.setAdapter(null);
        photoViewPager.onRestoreInstanceState(savedState);
        photoViewPager.setAdapter(bottomSheetItemPhotoAdapter);
        photoViewPager.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT);

        presenter.onItemSelected(bottomSheetItems.get(position));
    }

    public List<Integer> extractMarkersPhotos(List<IBottomSheetItem> bottomSheetItems) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < bottomSheetItems.size(); i++) {
            try {
                result.add(Utils.getResourceId(bottomSheetItems.get(i).getImages().get(0), R.drawable.class));
            } catch (Exception ex) {
                result.add(null);
            }
        }
        return result;
    }

    public List<String> extractMarkersCustomPhotos(List<IBottomSheetItem> bottomSheetItems) {
        List<String> result = new ArrayList<>();
        for (IBottomSheetItem bottomSheetItem : bottomSheetItems)
            if (MarkerType.MISSION.equals(bottomSheetItem.getType()))
                result.add(presenter.getCustomPhotoPath(bottomSheetItem.getId()));
            else
                result.add(null);

        return result;
    }

    public void synchronizeViewPagers(final ViewPager viewPager1, final ViewPager viewPager2) {
        viewPager1.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int scrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(int i, float v, int i1) {
                if (scrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
                viewPager2.scrollTo(viewPager1.getScrollX(), viewPager2.getScrollY());
            }

            @Override
            public void onPageSelected(int i) {
                position = i;
                presenter.onItemSelected(bottomSheetItems.get(i));
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                scrollState = i;
                if (i == ViewPager.SCROLL_STATE_IDLE) {
                    viewPager2.setCurrentItem(viewPager1.getCurrentItem(), false);
                }
            }
        });

        viewPager2.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int scrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (scrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
                viewPager1.scrollTo(viewPager2.getScrollX(), viewPager1.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                scrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPager1.setCurrentItem(viewPager2.getCurrentItem(), false);
                }
            }
        });
    }

    public void setFabMode() {
        if (this.bottomSheetItems != null && !this.bottomSheetItems.isEmpty()) {
            if (this.bottomSheetItems.get(0).getType().equals(MarkerType.MISSION)) {
                behavior.setFabEnabled(true);
            } else {
                behavior.setFabEnabled(false);
            }
        }
    }

    public void reloadContent(final List<IBottomSheetItem> iBottomSheetItems, int position) {
        this.bottomSheetItems = iBottomSheetItems;
        Log.d(TAG, "reloadContent, new bottomSheetItems: " + iBottomSheetItems.size());
        setFabMode();
        updateViewPagers(position);
        updateViewPagerHeightIfNeeded();
    }

    private void updateViewPagerHeightIfNeeded() {
        try {
            String name = bottomSheetItems.get(0).getName();
            ViewGroup.LayoutParams params = infoViewPager.getLayoutParams();
            if (name.equals("feeding_hours")
                    || name.equals("info")) {
                params.height = 4300;
            } else if (name.equals("opening_hours")) {
                params.height = 3800;
            } else if (name.equals("ticket")) {
                params.height = 7300;
            } else {
                params.height = 3300;
            }
            infoViewPager.setLayoutParams(params);
        } catch (Exception ignored) {}
    }

    private void initReferences() {
        this.bottomSheet = view.findViewById(R.id.bottom_sheet);
        this.floatingActionButton = view.findViewById(R.id.fab_bottom_sheet);
        this.infoViewPager = view.findViewById(R.id.pager);
        this.photoViewPager = view.findViewById(R.id.pager_photo);
        this.fabAugmentedReality = view.findViewById(R.id.fab_augmented_reality);
        setCustomPhotoDir();
        synchronizeViewPagers(infoViewPager, photoViewPager);
    }

    private void setupListeners() {
        fabAugmentedReality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onAugmentedRealityFABClick();
            }
        });
    }

    private void initFloatingActionButton() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runCameraFragment(bottomSheetItems.get(infoViewPager.getCurrentItem()).getId());
            }
        });
        floatingActionButton.setAlpha(0.0f);
    }

    public void setCustomPhotoDir() {
        presenter.setCustomPhotoDirectory(Objects.requireNonNull(getActivity()).getExternalCacheDir() + File.separator + MISSION_PHOTO_PATHS);
    }

    public void runCameraFragment(int markerID) { //starts Camera Fragment with number of Marker in Pager in Bundle
        Bundle bundle = new Bundle();
        bundle.putInt(MARKER_ID, markerID);
        MissionPhotoFragment missionPhotoFragment = new MissionPhotoFragment();
        missionPhotoFragment.setArguments(bundle);

        FragmentManager manager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.camera_mission_container, missionPhotoFragment, Objects.requireNonNull(getResources()).getString(R.string.fragment_tag_camera_mission));
        ft.commitAllowingStateLoss();
    }

    public void moveToPosition(final int position) {
        this.position = position;
        int infoPos = infoViewPager.getCurrentItem();
        int photoPos = infoViewPager.getCurrentItem();
        infoViewPager.setCurrentItem(position, Math.abs(position - infoPos) < 2 && isStateCollapsed());
        photoViewPager.setCurrentItem(position, Math.abs(position - photoPos) < 2 && isStateCollapsed());
        presenter.onItemSelected(bottomSheetItems.get(position));
    }

    @Override
    public void showBottomSheetItem(List<IBottomSheetItem> bottomSheetItems, Integer index) {
        Log.d(TAG, "showBottomSheetItem: called, index: " + index);
        if (bottomSheetItems == null || 0 > index || index >= bottomSheetItems.size())
            return;

        this.position = index;

        if (!bottomSheetItems.equals(this.bottomSheetItems)) {
            reloadContent(bottomSheetItems, index);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showBottomFragment();
                }
            }, LONG_HIDE_DURATION);
        } else {
            moveToPosition(index);
            showBottomFragment();
        }
    }

    @Override
    public void showBottomFragment() {
        Log.d(TAG, "showBottomFragment: called.");
        int state = presenter.getShowBottomFragment(bottomSheetItems.get(position));
        presenter.confirmBottomSheetShowing();
        behavior.setState(state);
    }

    @Override
    public void hideBottomFragment() {
        Log.d(TAG, "hideBottomFragment: called.");
        presenter.confirmBottomSheetHiding();
        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN);
    }

    @Override
    public void collapseBottomFragment() {
        Log.d(TAG, "collapseBottomFragment: called.");
        presenter.confirmBottomSheetShowing();
        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
    }

    @Override
    public void expandBottomFragment() {
        Log.d(TAG, "expandBottomFragment: called.");
        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT);
    }

    @Override
    public void setRoundedCorners(int radius, int radiusPhoto) {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(radius);
        GradientDrawable shapePhoto = new GradientDrawable();
        shapePhoto.setCornerRadius(radiusPhoto);

        infoViewPager.setClipToOutline(true);
        photoViewPager.setClipToOutline(true);

        infoViewPager.setBackground(shape);
        photoViewPager.setBackground(shapePhoto);
    }

    @Override
    public boolean isMissionAccomplished(Integer markerID) {
        return presenter.getCustomPhotoPath(markerID) != null;
    }

    @Override
    public void showFABAugmentedReality() {
        fabAugmentedReality.show();
    }

    @Override
    public void hideFABAugmentedReality() {
        fabAugmentedReality.hide();
    }

    @Override
    public void setBottomMarginFABAugmentedReality(int margin) {
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fabAugmentedReality.getLayoutParams();
        lp.bottomMargin = margin;
        fabAugmentedReality.setLayoutParams(lp);
        fabAugmentedReality.requestLayout();
    }

    @Override
    public void smoothScrollTo(int x, int y) {
        bottomSheet.smoothScrollTo(x, y);
    }

    @Override
    public void setPagingEnabled(Boolean isPageable) {
        photoViewPager.setPagingEnabled(isPageable);
        infoViewPager.setPagingEnabled(isPageable);
    }

    @Override
    public void setScrolling(boolean isScrollable) {
        behavior.setScrollable(isScrollable);
        bottomSheet.setScrollingEnabled(isScrollable);
        bottomSheet.setClickEnabled(isScrollable);
    }

    @Override
    public boolean isStateCollapsed() {
        return behavior.getState() == BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getFabMargin() {
        return fabMargin;
    }

}
