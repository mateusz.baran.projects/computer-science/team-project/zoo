package com.teamproject.zoo.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.teamproject.zoo.presenter.BottomSheetPresenter;

public class CustomNestedScrollView extends NestedScrollView {
    public static final long SHORT_DELAY = 200;

    private BottomSheetPresenter presenter;
    private boolean scrollable = true;
    private boolean clickable = true;

    public CustomNestedScrollView(@NonNull Context context) {
        super(context);
    }

    public CustomNestedScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomNestedScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (presenter != null && clickable && isShortClick(ev))
            presenter.click();
        return scrollable && super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (presenter != null && clickable && isShortClick(ev))
            presenter.click();
        return scrollable && super.onInterceptTouchEvent(ev);
    }

    private boolean isShortClick(MotionEvent ev) {
        return ev.getAction() == MotionEvent.ACTION_UP && ev.getEventTime() - ev.getDownTime() < SHORT_DELAY;
    }

    public void setPresenter(BottomSheetPresenter presenter) {
        this.presenter = presenter;
    }

    public void setScrollingEnabled(boolean enabled) {
        scrollable = enabled;
    }

    public void setClickEnabled(boolean enabled) {
        clickable = enabled;
    }

    public boolean isScrollingEnabled() {
        return scrollable;
    }
}
