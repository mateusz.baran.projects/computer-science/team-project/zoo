package com.teamproject.zoo.view;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.CheckboxTypeDto;
import com.teamproject.zoo.model.MarkerDto;
import com.teamproject.zoo.model.MarkerType;
import com.teamproject.zoo.model.SearchListItem;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.BottomSheetLayoutHelper;
import com.teamproject.zoo.presenter.SearchCheckboxListAdapter;
import com.teamproject.zoo.presenter.SearchListAdapter;
import com.teamproject.zoo.presenter.SearchPresenter;
import com.teamproject.zoo.presenter.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class SearchFragment extends Fragment implements BasePresenter.ISearchView {

    private static final int DURATION = 300;

    private TopSheetBehavior topSheetBehavior;
    private CoordinatorLayout searchFragment;
    private RecyclerView searchCheckboxList;
    private SearchPresenter presenter;
    private Context context;
    private View rootView;
    private SearchView searchView;
    private CoordinatorLayout searchTopSheet;
    private ListView searchListView;
    private List<SearchListItem> searchListItems;
    private MenuItem voiceSearchItem;
    private SpeechRecognizer speechRecognizer;
    private TextToSpeech textToSpeech;
    private List<CheckboxTypeDto> checkboxTypesDto;
    private static final int REQUEST_MICROPHONE = 300;


    private int fabMargin;
    private int barHeight;
    private int peekHeight;
    private int visiblePeek;

    private String filter;
    private boolean isActive;
    private boolean isAnimationDone;
    private boolean isEnable;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.presenter = new SearchPresenter(((MainActivity) Objects.requireNonNull(getActivity())).getBasePresenter(), this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);

        setupViews();

        filter = "";
        isAnimationDone = true;
        isEnable = false;

        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isAudioPermissionGranted()) {
            initializeSpeechRecognizer();
            initializeTextToSpeech();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (textToSpeech != null)
            textToSpeech.shutdown();
        if (speechRecognizer != null)
            speechRecognizer.destroy();
    }

    private void setupViews() {
        searchFragment = rootView.findViewById(R.id.search_fragment);
        searchCheckboxList = rootView.findViewById(R.id.search_checkbox_list_view);
        searchTopSheet = rootView.findViewById(R.id.search_top_sheet);
        searchListView = rootView.findViewById(R.id.search_list_view);
    }

    private void setupBehaviorIfNeeded() {
        if (topSheetBehavior == null) {
            visiblePeek = (int) getResources().getDimension(R.dimen.top_sheet_peek_height);
            fabMargin = (int) getResources().getDimension(R.dimen.fab_margin);
            barHeight = Utils.getStatusBarHeight(getResources()) + Utils.getToolbarBarHeight(context);
            int checkboxHeight = barHeight + searchCheckboxList.getHeight();
            peekHeight = checkboxHeight + visiblePeek;
            int listViewHeight = Utils.getWindowHeight(Objects.requireNonNull(getActivity()).getWindowManager()) - visiblePeek;


            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) searchCheckboxList.getLayoutParams();
            lp.topMargin = barHeight;
            searchCheckboxList.setLayoutParams(lp);
            searchCheckboxList.requestLayout();

            LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) searchFragment.getLayoutParams();
            lp2.topMargin = -peekHeight;
            searchFragment.setLayoutParams(lp2);
            searchFragment.requestLayout();
            searchFragment.setVisibility(View.VISIBLE);

            searchListView.setPadding(0, checkboxHeight, 0, 0);
            topSheetBehavior = TopSheetBehavior.from(searchTopSheet);
            topSheetBehavior.setPeekHeight(peekHeight);
            topSheetBehavior.setState(TopSheetBehavior.STATE_COLLAPSED);
            topSheetBehavior.setListView(searchListView, listViewHeight);

            topSheetBehavior.setTopSheetCallback(new TopSheetBehavior.TopSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == TopSheetBehavior.STATE_COLLAPSED)
                        presenter.onStateCollapsed();
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset, @Nullable Boolean isOpening) {
                    presenter.onSliding(slideOffset);
                }
            });

            setRoundedCorners(visiblePeek);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(Objects.requireNonNull(getActivity()).getString(R.string.search_query_hint));

        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        searchView.setFocusable(true);
        searchView.clearFocus();

        voiceSearchItem = menu.findItem(R.id.menu_voice_search_button);

        setupSearchViewListeners();
        setupVoiceSearch();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_MICROPHONE) {
            if (permissions.length == 1 &&
                    permissions[0].equals(Manifest.permission.RECORD_AUDIO) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initializeSpeechRecognizer();
                initializeTextToSpeech();
                runSpeechRecognizer();
            }
        }
    }

    private void setupVoiceSearch() {
        voiceSearchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (isAudioPermissionGranted() && speechRecognizer != null && textToSpeech != null)
                    runSpeechRecognizer();
                else {
                    initializeTextToSpeech();
                    initializeSpeechRecognizer();
                }
                return false;
            }
        });
    }

    private void setupSearchViewListeners() {
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchView.clearFocus();
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && isActive) {
                    if (!isEnable)
                        searchViewAnimator(true);

                    topSheetBehavior.setState(TopSheetBehavior.STATE_EXPANDED);
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filter = s;
                updateListView();
                return false;
            }
        });
    }

    @Override
    public void showSearchPanel(List<MarkerDto> markerDtos) {
        setupSearchListItemsIfNeeded(markerDtos);
        setupBehaviorIfNeeded();
        setupSearchCheckboxListViewAdapter();

        isActive = true;
        presenter.show();
    }

    @Override
    public void hideSearchPanel() {
        searchView.setQuery("", false);
        searchView.clearFocus();

        isActive = false;
        presenter.hide();
    }

    public void setupSearchListItemsIfNeeded(List<MarkerDto> markerDtos) {
        if (searchListItems == null) {
            searchListItems = new ArrayList<>();

            for (MarkerDto markerDto : markerDtos) {
                if (MarkerType.NORMAL.equals(markerDto.getType())) {
                    String title = BottomSheetLayoutHelper.getBottomSheetItemTitle(markerDto);
                    String habitat = BottomSheetLayoutHelper.getBottomSheetItemHabitat(markerDto);
                    Drawable drawable = null;
                    if (markerDto.getImages().size() > 0) {
                        int id = Utils.getResourceId(markerDto.getImages().get(0), R.drawable.class);
                        if (id > 0)
                            drawable = context.getDrawable(id);
                    }
                    searchListItems.add(new SearchListItem(markerDto.getId(), title, habitat, drawable));
                }
            }

            Collections.sort(searchListItems, new Comparator<SearchListItem>() {
                public int compare(SearchListItem a, SearchListItem b) {
                    return a.getTitle().compareTo(b.getTitle());
                }
            });

            updateListView();
        }
    }

    private void updateListView() {
        if (searchListItems != null) {
            List<SearchListItem> filteredSearchListItems = new ArrayList<>();
            for (SearchListItem searchListItem : searchListItems) {
                if (searchListItem.getTitle().toLowerCase().startsWith(filter)
                        || searchListItem.getHabitat().toLowerCase().startsWith(filter)) {
                    filteredSearchListItems.add(searchListItem);
                }
            }

            final SearchListAdapter adapter = new SearchListAdapter(filteredSearchListItems, getContext());
            searchListView.setAdapter(adapter);

            searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    presenter.onListItemClick(adapter.getMarkerDtoID(position));
                    searchView.clearFocus();
                }
            });
        }
    }

    public void setupSearchCheckboxListViewAdapter() {
        SearchCheckboxListAdapter adapter = new SearchCheckboxListAdapter(checkboxTypesDto, getContext(), presenter);
        searchCheckboxList.setAdapter(adapter);
    }

    @Override
    public void setupCheckboxListView(List<CheckboxTypeDto> checkboxTypeDtos) {
        this.checkboxTypesDto = checkboxTypeDtos;

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        searchCheckboxList.setLayoutManager(layoutManager);

        setupSearchCheckboxListViewAdapter();
    }

    @Override
    public void searchViewAnimator(final boolean enable) {
        if (isEnable != enable && isAnimationDone && (!enable || isActive)) {
            isEnable = enable;
            isAnimationDone = false;

            ValueAnimator anim = !enable ? ValueAnimator.ofFloat(0, 1) : ValueAnimator.ofFloat(1, 0);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float progress = (float) valueAnimator.getAnimatedValue();

                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) searchFragment.getLayoutParams();
                    params.topMargin = (int) -(peekHeight * progress);
                    searchFragment.setLayoutParams(params);
                    searchFragment.requestLayout();

                    presenter.manageFABMyLocation(peekHeight + params.topMargin, barHeight, fabMargin);
                }
            });

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    isAnimationDone = true;
                }
            });

            anim.setDuration(DURATION);
            anim.start();
        }
    }

    @Override
    public void setRoundedCorners(int radius) {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(radius);

        searchTopSheet.setClipToOutline(true);

        searchTopSheet.setBackground(shape);
    }

    @Override
    public void collapseListView() {
        topSheetBehavior.setState(TopSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public int getVisiblePeek() {
        return visiblePeek;
    }

    private void processResult(String result) {
        searchView.setQuery(result.toLowerCase(), false);
    }

    protected void runSpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, getString(R.string.search_pl_locale));
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        speechRecognizer.startListening(intent);
    }

    private void initializeTextToSpeech() {
        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (textToSpeech.getEngines().size() == 0) {
                    Toast.makeText(getContext(), getString(R.string.search_error_no_engine_installed), Toast.LENGTH_LONG).show();
                } else if (status == TextToSpeech.SUCCESS) {
                    Locale locale = Locale.forLanguageTag(getString(R.string.search_pl));
                    textToSpeech.setLanguage(locale);
                }
            }
        });
    }

    private boolean isAudioPermissionGranted() {
        return ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }


    private void initializeSpeechRecognizer() {
        if (SpeechRecognizer.isRecognitionAvailable(getContext())) {

            if (!isAudioPermissionGranted()) {
                ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        REQUEST_MICROPHONE);

            } else {
                speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getContext());
                speechRecognizer.setRecognitionListener(new RecognitionListener() {
                    @Override
                    public void onReadyForSpeech(Bundle params) {
                    }

                    @Override
                    public void onBeginningOfSpeech() {

                    }

                    @Override
                    public void onRmsChanged(float rmsdB) {

                    }

                    @Override
                    public void onBufferReceived(byte[] buffer) {

                    }

                    @Override
                    public void onEndOfSpeech() {

                    }

                    @Override
                    public void onError(int error) {
                        String message;
                        switch (error) {
                            case SpeechRecognizer.ERROR_AUDIO:
                                message = getString(R.string.search_error_audio_recognition_error);
                                break;
                            case SpeechRecognizer.ERROR_CLIENT:
                                message = getString(R.string.search_error_client_side_error);
                                break;
                            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                                message = getString(R.string.search_error_insufficient_permissions);
                                break;
                            case SpeechRecognizer.ERROR_NETWORK:
                                message = getString(R.string.search_error_network_error);
                                break;
                            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                                message = getString(R.string.search_error_network_timeout);
                                break;
                            case SpeechRecognizer.ERROR_NO_MATCH:
                                message = getString(R.string.search_error_no_match);
                                break;
                            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                                message = getString(R.string.search_error_recognizer_busy);
                                break;
                            case SpeechRecognizer.ERROR_SERVER:
                                message = getString(R.string.search_error_from_server);
                                break;
                            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                                message = getString(R.string.search_error_speech_timeout);
                                break;
                            default:
                                message = getString(R.string.search_error_did_not_understand);
                                break;
                        }
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResults(Bundle results) {
                        List<String> words = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                        if (words != null) {
                            processResult(words.get(0));
                        } else {
                            Toast.makeText(getContext(), getString(R.string.search_error_something_goes_wrong), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPartialResults(Bundle partialResults) {

                    }

                    @Override
                    public void onEvent(int eventType, Bundle params) {

                    }
                });
            }
        } else {
            Toast.makeText(context, getString(R.string.search_error_not_available), Toast.LENGTH_SHORT).show();
        }
    }

}
