package com.teamproject.zoo.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Toast;

import com.teamproject.zoo.R;
import com.teamproject.zoo.model.RouteDto;
import com.teamproject.zoo.presenter.BasePresenter;
import com.teamproject.zoo.presenter.RouteSelectorAdapter;
import com.teamproject.zoo.presenter.RouteSelectorPresenter;

import java.util.List;
import java.util.Objects;

public class RouteSelectorFragment extends Fragment implements BasePresenter.IRouteView {
    private ViewPager routeSelectorViewPager;
    private CheckBox missionCheckBox;
    private boolean isMissionOn;
    private List<RouteDto> routes;

    private FloatingActionButton confirmRouteButton;
    private ImageButton viewPagerLeftArrow;
    private ImageButton viewPagerRightArrow;
    private RouteSelectorPresenter presenter;

    //flag to prevent setupping selector every time it shows
    private boolean isSetup = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity mainActivity = ((MainActivity) Objects.requireNonNull(getActivity()));
        this.presenter = new RouteSelectorPresenter(mainActivity.getBasePresenter(), this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_route_selector, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setupRouteSelector(final List<RouteDto> rts) {
        routes = rts;
        isSetup = true;
        final RouteSelectorAdapter routeSelectorAdapter = new RouteSelectorAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), routes);
        routeSelectorViewPager = getActivity().findViewById(R.id.route_selector);
        confirmRouteButton = getActivity().findViewById(R.id.button_confirm_route);
        missionCheckBox = getActivity().findViewById(R.id.checkBox_missions);
        viewPagerLeftArrow = getActivity().findViewById(R.id.view_pager_left_arrow);
        viewPagerRightArrow = getActivity().findViewById(R.id.view_pager_right_arrow);
        routeSelectorViewPager.setAdapter(routeSelectorAdapter);

        missionCheckBox.setChecked(presenter.isMissionActive());

        confirmRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onConfirmRoute();
            }
        });

        missionCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                       @Override
                                                       public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                           presenter.setMissionActive(isChecked);
                                                       }
                                                   }
        );

        viewPagerLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeSelectorViewPager.setCurrentItem(routeSelectorViewPager.getCurrentItem() - 1);
            }
        });

        viewPagerRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeSelectorViewPager.setCurrentItem(routeSelectorViewPager.getCurrentItem() + 1);
            }
        });

        routeSelectorViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                presenter.showRoute(routes.get(i).getId());
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        // hide route selector on swipe down event
//        ImageView area = getActivity().findViewById(R.id.route_selector_onTouch_area);
//        area.bringToFront();
//        area.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    presenter.hideRouteSelector();
//                }
//                return false;
//            }
//        });
    }

    @SuppressLint("RestrictedApi")
    public void showRouteSelector(List<RouteDto> routes, Integer index) {
        if (routeSelectorViewPager == null) {
            setupRouteSelector(routes);
        }

        if (index == null) {
            Toast.makeText(getContext().getApplicationContext(), "Brak tras", Toast.LENGTH_SHORT).show();
            return;
        }

        routeSelectorViewPager.setCurrentItem(index);
        presenter.showRoute(routes.get(index).getId());

        Animation bottomUp = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_up);

        bottomUp.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                presenter.mapDeltaHeight(getActivity().findViewById(R.id.route_selector).getHeight());
            }
        });

        routeSelectorViewPager.startAnimation(bottomUp);
        confirmRouteButton.startAnimation(bottomUp);
        missionCheckBox.startAnimation(bottomUp);
        viewPagerLeftArrow.startAnimation(bottomUp);
        viewPagerRightArrow.startAnimation(bottomUp);
        routeSelectorViewPager.setVisibility(View.VISIBLE);

        confirmRouteButton.setVisibility(View.VISIBLE);
        missionCheckBox.setVisibility(View.VISIBLE);
        viewPagerLeftArrow.setVisibility(View.VISIBLE);
        viewPagerRightArrow.setVisibility(View.VISIBLE);
    }

    @SuppressLint("RestrictedApi")
    public void hideRouteSelector() {
        Animation bottomDown = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_down);

        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                presenter.mapDeltaHeight(0);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }
        });


        routeSelectorViewPager.startAnimation(bottomDown);
        confirmRouteButton.startAnimation(bottomDown);
        missionCheckBox.startAnimation(bottomDown);
        viewPagerLeftArrow.startAnimation(bottomDown);
        viewPagerRightArrow.startAnimation(bottomDown);
        routeSelectorViewPager.setVisibility(View.GONE);

        confirmRouteButton.setVisibility(View.GONE);
        missionCheckBox.setVisibility(View.GONE);
        viewPagerLeftArrow.setVisibility(View.GONE);
        viewPagerRightArrow.setVisibility(View.GONE);
    }

    public void setMissionOn(boolean missionOn) {
        isMissionOn = missionOn;
    }

    public boolean isMissionOn() {
        return isMissionOn;
    }

    public boolean isSetup() {
        return isSetup;
    }
}
