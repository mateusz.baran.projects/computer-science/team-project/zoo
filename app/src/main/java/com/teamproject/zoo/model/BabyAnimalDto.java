package com.teamproject.zoo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BabyAnimalDto {
    private int id;
    private String name;
    private String animalName;
    private String description;
    private String image;
}
