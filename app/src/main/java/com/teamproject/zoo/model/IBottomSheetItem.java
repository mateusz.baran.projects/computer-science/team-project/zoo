package com.teamproject.zoo.model;

import java.util.List;
import java.util.Map;

public interface IBottomSheetItem {

    int getId();

    String getName();

    String getType();

    Map<String, Object> getLayouts();

    List<String> getImages();
}
