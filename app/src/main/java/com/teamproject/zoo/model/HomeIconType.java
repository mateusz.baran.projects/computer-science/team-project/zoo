package com.teamproject.zoo.model;

public final class HomeIconType {
    public static final int HAMBURGER = 1;
    public static final int BACK_ARROW = 2;

    private HomeIconType() {
    }
}
