package com.teamproject.zoo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarkerAR {
    public static final String ICON_DEFAULT = "marker_background";
    public static final String ICON_LEFT_ARROW = "ic_round_arrow_left";
    public static final String ICON_RIGHT_ARROW = "ic_round_arrow_right";
    public static final String NEARLY = "W pobliżu";

    public static final int MAX_SIZE = 64;
    public static final int MIN_SIZE = 24;
    public static final float MAX_OPACITY = .8f;
    public static final float MIN_OPACITY = .4f;

    private MarkerDto marker;
    private String title;
    private String habitat;
    private String subtitle;
    private String icon;
    private float iconSize;
    private int marginLeft;
    private int marginTop;
    private float alpha;
    private float elevation;
    private boolean visible;
    private double distance;
    private double bearing;

    public MarkerAR(MarkerDto marker) {
        this.marker = marker;
    }
}
