package com.teamproject.zoo.model;

public final class ToolbarMode {

    public final static int NORMAL = 1;
    public final static int ROUTE_SELECTION = 2;
    public final static int BOTTOM_PANEL_EXPANDED_STATE_INFO = 3;
    public final static int BOTTOM_PANEL_MIDDLE_STATE_INFO = 4;
    public final static int PARKING = 5;
    public final static int AUGMENTED_REALITY = 6;
    public final static int SEARCH = 7;
    public final static int FIND_CAR = 8;

    private ToolbarMode() {
    }
}
