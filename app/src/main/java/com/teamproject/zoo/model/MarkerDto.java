package com.teamproject.zoo.model;

import com.google.firebase.firestore.GeoPoint;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarkerDto implements IBottomSheetItem {
    private int id;
    private String name;
    private GeoPoint location;
    private Map<String, Object> anchor;
    private String type;
    private String group;
    private Map<String, Object> layouts;
    private Map<String, Float> zoom;
    private Map<String, Object> icon;
    private List<String> images;
    private Integer missionMaxScore;
    private String missionType;
}
