package com.teamproject.zoo.model;

import android.graphics.drawable.Drawable;

public class SearchListItem {
    private Integer id;
    private String title;
    private String habitat;
    private Drawable image;

    public SearchListItem(Integer id, String title, String habitat, Drawable image) {
        this.id = id;
        this.title = title;
        this.habitat = habitat;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getHabitat() {
        return habitat;
    }

    public Drawable getImage() {
        return image;
    }
}
