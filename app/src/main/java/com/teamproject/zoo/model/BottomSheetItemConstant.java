package com.teamproject.zoo.model;

public final class BottomSheetItemConstant {
    public static final String LAYOUTS_BOTTOM_SHEET = "bottomSheet";
    public static final String IMAGES = "images";
    public static final String CONTENTS = "contents";
    public static final String LAYOUTS_ID = "id";
    public static final String TITLE_KEY = "bottom_sheet_item_title_bar_title";
    public static final String ANIMAL_HABITAT_KEY = "textView_animal_habitat";

    private BottomSheetItemConstant() {}
}
