package com.teamproject.zoo.model;

import com.google.firebase.firestore.GeoPoint;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RouteDto {
    private int id;
    private String name;
    private String description;
    private int distance;
    private int time;
    private boolean disabled;
    private boolean children;
    private List<GeoPoint> points;
    private String group;
}
