package com.teamproject.zoo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FeedingHoursDto {
    private int id;
    private String name;
    private String hour;
    private String animals;
    private String place;
}