package com.teamproject.zoo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckboxTypeDto {
    private String type;
    private String image;
    private boolean isChecked;
    private String title;
}
