package com.teamproject.zoo.model;

public class ZooMap {
    public final float MAP_WIDTH = 1200;
    public final float MAP_BEARING = 208.0f;

    // TODO check twice below constants
    public final float MAP_ZOOM_ZONE_MIN = 15.0f;
    public final float MAP_ZOOM_ZONE_MAX = 17.0f;
    public final float SOUTH_WEST_MAP_CORNER_LAT = 51.100167f;
    public final float SOUTH_WEST_MAP_CORNER_LNG = 17.067593f;
    public final float NORTH_EAST_MAP_CORNER_LAT = 51.108239f;
    public final float NORTH_EAST_MAP_CORNER_LNG = 17.080490f;
    public final float CENTER_MAP_POINT_LAT = 51.104266f;
    public final float CENTER_MAP_POINT_LNG = 17.074904f;

    public ZooMap() {

    }
}
