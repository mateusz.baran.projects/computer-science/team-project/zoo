package com.teamproject.zoo.model;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavDrawerDto implements IBottomSheetItem {
    private int id;
    private String name;
    private String type;
    private Map<String, Object> layouts;
    private List<String> images;
}
