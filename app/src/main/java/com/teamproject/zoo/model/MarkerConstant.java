package com.teamproject.zoo.model;

public final class MarkerConstant {
    private MarkerConstant(){}
    public static final String FB_COLLECTION_PATH = "markers";
    public static final String ANCHOR_X = "x";
    public static final String ANCHOR_Y = "y";
    public static final String ICON_ACTIVE = "active";
    public static final String ICON_INACTIVE = "inactive";
    public static final String ZOOM_ACTIVATION = "activation";
    public static final String ZOOM_DEACTIVATION = "deactivation";
    public static final String DEFAULT_MARKER = "marker_background";
    public static final int ICON_ACTIVE_SIZE = 50;
    public static final int ICON_INACTIVE_SIZE = 44;
}
