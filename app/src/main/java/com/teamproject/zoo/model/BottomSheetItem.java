package com.teamproject.zoo.model;

import com.teamproject.zoo.presenter.BasePresenter;

import java.util.List;
import java.util.Map;

public class BottomSheetItem implements IBottomSheetItem {

    private int id;
    private String name;
    private String type;
    private Map<String, Object> layouts;
    private List<String> images;

    public BottomSheetItem(int id, String name, String type, Map<String, Object> layouts, List<String> images) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.layouts = layouts;
        this.images = images;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public Map<String, Object> getLayouts() {
        return layouts;
    }

    @Override
    public List<String> getImages() {
        return images;
    }
}
