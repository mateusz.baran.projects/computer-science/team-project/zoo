package com.teamproject.zoo.model;

public final class MarkerType {

    public final static String NORMAL = "normal";
    public final static String MISSION = "mission";
    public final static String PARKING = "parking";
    public final static String TICKET = "ticket";
    public final static String BENCH = "bench";
    public final static String ICE_CREAM = "ice_cream";
    public final static String TOILET = "toilet";
    public final static String PAVILION = "pavilion";
    public final static String MAIN_POINT = "main_point";
    public final static String NONE = "none";

    private MarkerType() {
    }
}
