package com.teamproject.zoo;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.teamproject.zoo.dao.ContentDao;
import com.teamproject.zoo.dao.ImageDao;
import com.teamproject.zoo.dao.MarkerAnchorDao;
import com.teamproject.zoo.dao.MarkerDao;
import com.teamproject.zoo.dao.MarkerGroupDao;
import com.teamproject.zoo.dao.MarkerIconDao;
import com.teamproject.zoo.dao.MarkerIconTypeDao;
import com.teamproject.zoo.dao.MarkerSectionContentDao;
import com.teamproject.zoo.dao.MarkerSectionDao;
import com.teamproject.zoo.dao.MarkerTypeDao;
import com.teamproject.zoo.dao.MissionDao;
import com.teamproject.zoo.dao.PointDao;
import com.teamproject.zoo.dao.RouteDao;
import com.teamproject.zoo.dao.RouteMissionDao;
import com.teamproject.zoo.dao.RoutePointDao;
import com.teamproject.zoo.dao.SectionDao;
import com.teamproject.zoo.database.ZooDatabase;
import com.teamproject.zoo.entity.Content;
import com.teamproject.zoo.entity.Marker;
import com.teamproject.zoo.entity.MarkerGroup;
import com.teamproject.zoo.entity.MarkerSection;
import com.teamproject.zoo.entity.MarkerSectionContent;
import com.teamproject.zoo.entity.MarkerType;
import com.teamproject.zoo.entity.Mission;
import com.teamproject.zoo.entity.Point;
import com.teamproject.zoo.entity.Route;
import com.teamproject.zoo.entity.RouteMission;
import com.teamproject.zoo.entity.RoutePoint;
import com.teamproject.zoo.entity.Section;
import com.teamproject.zoo.model.MarkerInfo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(AndroidJUnit4.class)
public class DatabaseUnitTest {
    private ZooDatabase db;
    private ContentDao contentDao;
    private ImageDao imageDao;
    private MarkerDao markerDao;
    private MarkerAnchorDao markerAnchorDao;
    private MarkerGroupDao markerGroupDao;
    private MarkerIconDao markerIconDao;
    private MarkerIconTypeDao markerIconTypeDao;
    private MarkerSectionDao markerSectionDao;
    private MarkerSectionContentDao markerSectionContentDao;
    private MarkerTypeDao markerTypeDao;
    private MissionDao missionDao;
    private PointDao pointDao;
    private RouteDao routeDao;
    private RouteMissionDao routeMissionDao;
    private RoutePointDao routePointDao;
    private SectionDao sectionDao;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext().getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, ZooDatabase.class).build();
        contentDao = db.contentDao();
        markerDao = db.markerDao();
        imageDao = db.imageDao();
        markerAnchorDao = db.markerAnchorDao();
        markerGroupDao = db.markerGroupDao();
        markerIconDao = db.markerIconDao();
        markerIconTypeDao = db.markerIconTypeDao();
        markerSectionDao = db.markerSectionDao();
        markerSectionContentDao = db.markerSectionContentDao();
        markerTypeDao = db.markerTypeDao();
        missionDao = db.missionDao();
        pointDao = db.pointDao();
        routeDao = db.routeDao();
        routeMissionDao = db.routeMissionDao();
        routePointDao = db.routePointDao();
        sectionDao = db.sectionDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeMarkerAndRead() throws Exception {
        Marker marker = new Marker();
        marker.setZoomActivation(100.23F);
        marker.setZoomDeactivation(50.36F);
        markerDao.insert(marker);
        Marker temp = markerDao.get(1L);
        assertThat(temp.getIdMarker(), equalTo(1));
        assertThat(temp.getZoomActivation(), equalTo(100.23F));
        assertThat(temp.getZoomDeactivation(), equalTo(50.36F));
    }

    @Test
    public void writeDataAndReadMarkerTemp() throws Exception {
        LinkedList<MarkerType> markerTypes = new LinkedList<>();
        LinkedList<MarkerGroup> markerGroups = new LinkedList<>();
        LinkedList<Section> sections = new LinkedList<>();
        LinkedList<Content> contents = new LinkedList<>();
        LinkedList<Mission> missions = new LinkedList<>();
        LinkedList<Point> points = new LinkedList<>();
        LinkedList<Route> routes = new LinkedList<>();
        LinkedList<RoutePoint> routePoints = new LinkedList<>();
        LinkedList<Marker> markers = new LinkedList<>();
        LinkedList<RouteMission> routeMissions = new LinkedList<>();
        LinkedList<MarkerSection> markerSections = new LinkedList<>();
        LinkedList<MarkerSectionContent> markerSectionContents = new LinkedList<>();

        markerTypes.add(new MarkerType(1, "mission"));
        markerTypes.add(new MarkerType(2, "normal"));
        markerTypes.add(new MarkerType(3, "normal"));

        markerGroups.add(new MarkerGroup(1, "take_selfie", "Zrób selfie"));
        markerGroups.add(new MarkerGroup());
        markerGroups.add(new MarkerGroup());
        markerGroups.add(new MarkerGroup());
        markerGroups.add(new MarkerGroup());

        sections.add(new Section(1, "Opis", "R.layout.layout_test_1"));
        sections.add(new Section(2, "Godziny otwarcia", "R.layout.layout_test_2"));
        sections.add(new Section(3, "Zdjecia", "R.layout.layout_test_3"));

        contents.add(new Content(1, "R.id.bottom_sheet_title", "To jest opis1."));
        contents.add(new Content(2, "R.id.bottom_sheet_subtitle", "Opis drugi jara szlugi."));
        contents.add(new Content(3, "R.id.bottom_sheet_title", "Opis trzeciGMD"));
        contents.add(new Content(4, "R.id.bottom_sheet_subtitle", "Opis czwarty chujowo gra w karty"));

        //missionTypes.add(new MissionType(1, "take_selfie", "Zrób selfie"));
        //missionTypes.add(new MissionType(2, "scan_code_qr", "Zeskanuj kod QR"));

        points.add(new Point(1, 51.104607f, 17.077376f));
        points.add(new Point(2, 51.103231f, 17.075175f));
        points.add(new Point(3, 51.103651f, 17.072549f));
        points.add(new Point(4, 51.105301f, 17.072529f));
        points.add(new Point(5, 51.104993f, 17.077453f));
        points.add(new Point(6, 51.104840f, 17.077784f));
        points.add(new Point(7, 51.104840f, 17.077784f));

        routes.add(new Route(1, "Królestwo Małp", "", 5, 90, "friendly", "friendly"));
        routes.add(new Route(2, "Zwierzęta Europy", "", 6, 80, "unfriendly", "unfriendly"));

        routePoints.add(new RoutePoint(1, 1, 1));
        routePoints.add(new RoutePoint(1, 3, 2));
        routePoints.add(new RoutePoint(1, 4, 3));
        routePoints.add(new RoutePoint(2, 2, 1));
        routePoints.add(new RoutePoint(2, 5, 2));
        routePoints.add(new RoutePoint(2, 6, 3));
        routePoints.add(new RoutePoint(2, 7, 4));

        markers.add(new Marker(1, 2, 1, null, null, 15.0f, 16.5f, null));
        markers.add(new Marker(2, 2, 2, null, null, 15.5f, 17.0f, null));
        markers.add(new Marker(3, 1, 3, null, 1, 0f, 100f, " ze sloniem"));
        markers.add(new Marker(4, 1, 4, null, 1, 0f, 100f, " z gibonem"));
        markers.add(new Marker(5, 1, 5, null, 1, 0f, 100f, " z Januszem"));
        markers.add(new Marker(6, 1, 6, null, 1, 0f, 100f, " z Nosaczem"));
        markers.add(new Marker(7, 1, 7, null, 1, 0f, 100f, " z Grazyna"));

        missions.add(new Mission(1, 1, 2));
        missions.add(new Mission(2, 1, 3));
        missions.add(new Mission(3, 2, 1));
        missions.add(new Mission(4, 1, 3));
        missions.add(new Mission(5, 2, 4));

        routeMissions.add(new RouteMission(1, 1, null, false, 0));
        routeMissions.add(new RouteMission(1, 2, null, false, 0));
        routeMissions.add(new RouteMission(2, 3, null, false, 0));
        routeMissions.add(new RouteMission(2, 4, null, false, 0));
        routeMissions.add(new RouteMission(2, 5, null, false, 0));

        markerSections.add(new MarkerSection(1, 1, 1));
        markerSections.add(new MarkerSection(2, 1, 2));
        markerSections.add(new MarkerSection(3, 2, 1));
        markerSections.add(new MarkerSection(4, 2, 2));

        markerSectionContents.add(new MarkerSectionContent(1, 1));
        markerSectionContents.add(new MarkerSectionContent(2, 2));
        markerSectionContents.add(new MarkerSectionContent(3, 3));
        markerSectionContents.add(new MarkerSectionContent(4, 4));

        sectionDao.insertAll(sections);
        markerTypeDao.insertAll(markerTypes);
        markerGroupDao.insertAll(markerGroups);
        contentDao.insertAll(contents);
        routeDao.insertAll(routes);
        pointDao.insertAll(points);
        markerDao.insertAll(markers);
        missionDao.insertAll(missions);
        routePointDao.insertAll(routePoints);
        routeMissionDao.insertAll(routeMissions);
        markerSectionDao.insertAll(markerSections);
        markerSectionContentDao.insertAll(markerSectionContents);

        ArrayList<Marker> markers2 = (ArrayList<Marker>) markerDao.getAll();
        ArrayList<MarkerInfo> markerInfos = (ArrayList<MarkerInfo>) markerDao.getAllMarkers();
        assertThat(markers2.size(), equalTo(7));
        assertThat(markerInfos.size(), equalTo(7));
        assertThat(markerDao.get(1L).getIdMarkerType(), equalTo(2));
        assertThat(markerDao.get(1L).getIdPoint(), equalTo(1));
        assertThat(markerDao.get(1L).getZoomActivation(), equalTo(15.0f));
        assertThat(markerDao.get(1L).getZoomDeactivation(), equalTo(16.5f));
    }
}
